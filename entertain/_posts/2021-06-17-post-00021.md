---
layout: post
title: "[A.A.F] 감동(드라마), 청춘 애니 추천(feat. azunyaa)"
toc: true
---

 

 이득 글은 도시 주관적이고
 개인적인 평가입니다.
 

 

 - 추천 순서는 가나다 순입니다.
- 평가는 작화(연출, 캐릭터디자인,작붕...), 스토리, 캐릭터성 3가지 면에서 하도록 하겠습니다.
- 약간의 스포일러의 가능성이 있습니다.
- 평점은 10점이 만점입니다.
- 원작과 별개로 애니메이션 자체만 평가합니다.
2021.04.28 1차
 고시 기준
· 10/10 : 격 관계없이 애니를 좋아하는 누구에게나 추천하고 싶은 작품
·   9/10 : 관련 장르를 대표하는 적극적으로 추천하는 작품
·   8/10 : 연관 장르를 경험해 보고 싶으신 분에게 추천하는 작품
·   7/10 : 해당 장르를 좋아하는 사람에게만 추천하는 작품
 

 ※ 최대한 다른 장르별 작품과 겹치지 않도록 선정하였다.
※ 감동으로 정의했지만 장르로써는 희곡 장르에 해당합니다.

 목록
  .
 1. 그 여름에서 기다릴게
 2. 그날 본 꽃의 이름을 우리는 아직 모른다. (아노하나)
 3. 꽃이 피는 첫걸음
 4. 나츠메 우인장
 5. 다다미 넉 장 반 세계일주
 6. 도쿄 매그니튜드 8.0
 7. 리틀 버스터즈! (+ ~Refrain~)
 8. 물드는 세계의 내일로부터
 9. 바라카몬
 10. 바이올렛 에버가든
 11. 바쿠만
 12. 배를 엮다
 13. 변태왕자와 웃지 않는 고양이
 14. 비스타즈(+ 2기)
 15. 사랑은 비가 갠 뒤처럼
 16. 사쿠라장의 애완 그녀
 17. 소녀 종말 여행
 18. 쇼와 겐로쿠 라쿠고 신쥬
 19. 시로바코
 20. 암살교실 (+ 세컨드, 파이널 시즌)
 21. 언덕길의 아폴론
 22. 역시 내 청춘 러브코메디는 잘못됐다. (+ 속, 완)
 23. 영상연에는 손대지마
 24. 우주보다 먼 곳
 25. 울려라! 유포니엄 (+ 2기)
 26. 원더 에그 프라이어리티
 27. 은수저
 28. 작은 별의 플라네타리안
 29. 저 너머의 아스트라
 30. 종말에 뭐하세요 바쁘세요 구해주실 수 있나요
 31. 청춘 돼지는 바니걸 선배의 꿈을 꾸지 않는다
 32. 충사 (+ 속장)
 33. 카우보이 비밥
 34. 크게 휘두르며
 35. 클라나드 (+ AFTER STORY)
 36. 키노의 여행 -the beautiful world-
 37. 토끼 드롭스
 38. 토라도라
 39. 플라스틱 메모리즈
 40. 핑퐁
 41. 하트 커넥트
 42. 허니와 클로버
 43. Angel Beats !
 44. ReLIFE
 45. 3월의 라이온
 46. 4월은 너의 거짓말
 

 

## 1. 그 여름에서 기다릴게
 도식 - 드라마, 청춘, 학원
 제작사 - J.C.STAFF
 

 나쁘지 않은 청춘물. 등장인물들의 사춘기 감성을 다루는 청춘물입니다. 잔잔한 구리 배경에 우주인이라는 약간의 스파이스를 더한 스토리는 무지무지 지루해지지 않아 좋았습니다. 후반 영화라는 소재를 이용한 패러디가 억지스럽다는 평도 있지만 개인적으로는 나름 전체적인 흐름이 끊어지지 않게 마무리하였다고 생각합니다. 청춘 극 관계 장르에 입문하시는 분에게 추천하는 작품입니다.
 

 8/10 : 파릇파릇한 청춘을 느껴보고 싶으신 분에게 추천한다.
 

## 2. 그날 본 꽃의 이름을 우리는 아직 모른다. (아노하나)
 형식 - 드라마, 청춘
 제작사 - A-1 Pictures
 

 음악이 멱살 캐리. 어릴 도리 겪은 친구의 갑작스러운 죽음으로 트라우마를 가지고 있는 등장인물들이 유령으로 돌아온 친구를 성불시키면서 성장하는 스토리입니다. 감동을 느끼도록 유도하는 구성을 가지고 있고 각자의 트라우마에 공감하기 힘든 부분도 존재하지만 애당초 적절한 타이밍에 엔딩곡이 깔려 감정을 흔들어 놓습니다. 후반 전개도 급하고 한결 섬세하게 청소년의 고민을 다루는 작품들이 존재하지만 인상적인 작품을 고르라면 그냥 적으로 고르게 되는 그런 묘한 작품입니다.
 

 8/10 : 다른 미디어에서는 느껴볼 수 없는 애니메이션의 감동을 느껴보고 싶으신 분에게 추천한다.
 

## 3. 꽃이 피는 첫걸음
 요식 - 드라마, 청춘
 제작사 - P.A.WORKS
 

 시로바코의 열화판. 여관에서 일하는 여고생의 일상을 다루는 작품입니다. 여관이라는 소재도 나쁘지 않았고 여관이라는 직업의 어려움도 방금 표현되어 있습니다. P.A 오리지널 애니메이션 답게 스토리의 완성도도 나쁘지 않았으나 초반 막장 드라마 전개와 약간의 짜증을 일으키는 캐릭터들의 성격이 한결 좋은 평가를 가로막았습니다. 청춘 근로 장르를 좋아하시는 분에게 추천하는 작품입니다.
 

 7/10 : 특정 직업에서 일하는 내용의 작품을 좋아하시는 분에게 추천한다.
 

## 4. 나츠메 우인장
 형식 - 드라마
 제작사 - 브레인즈 베이스, 슈카
 

 도량 따뜻해지는 치유물. 잔잔하고 부드러운 분위기의 작품입니다. 작품에 등장하는 요괴는 인간처럼 감정을 가지고 있는 이형적이지만 친숙한 존재로 그려집니다. 옴니버스 형식의 에피소드들이 하나하나가 마음을 울립니다. 고작해야 동시에 단편 에피소드들로만 이루어져 있기 그렇게 몰아보기에는 지루한 경향이 있습니다. 진실 잔잔한 감동물을 찾으시는 분에게 추천합니다.
 

 8/10 : 감동물을 지루하다고 느끼지 않는 사람이라면 추천한다.
 

## 5. 다다미 넉 장 반 세계일주
 폼 - 청춘, 드라마
 [왓챠](https://quarrel-position.gq/entertain/post-00002.html) 제작사 - 매드 하우스
 

 진짜 청춘 이야기. 청춘을 마치 귀중한 것 마냥 다루는 여러 작품과 달리 청춘이라는 소재를 매우 담담하게 풀어나가고 있습니다. 평행세계 이야기 구조에 소설을 애니화한 작품이라서 대사량도 매우 많아 집중해서 보지 않으면 이야기의 흐름을 놓칠 정도입니다. 참 보잘것없는 평범한 주인공이기에 전달하는 주제가 너무나도 와 닿는 그런 작품이었습니다.
 

 9/10 : 청춘이라고 불리는 나이이신 분에게 추천한다.
 

## 6. 도쿄 매그니튜드 8.0
 구성 - 재해, 드라마
 제작사 - bones
 

 재난 애니. 재난을 다루는 애니는 대부분 이문 작품이 유일하다고 할 이운 있습니다. 실지로 지진이 자주 일어나는 나라라서 그런지 등장인물들의 반응이 현실적입니다. 오히려 복선들은 뻔한 감이 있어서 중후반부터 어느 수동레벨 결말을 예측할 행운 있는 점은 아쉽습니다. 일반적인 재난 영화보다는 플레이 타임이 길기 왜냐하면 후반 비교적 뭉클합니다. 재난 영화를 좋아하시는 분이라면 적극 추천하는 작품입니다.
 

 8/10 : 재난 영화를 좋아하시는 분에게 추천한다.
 

## 7. 리틀 버스터즈! (+ ~Refrain~)
 상 - 학원, 청춘, 드라마
 제작사 - J.C.STAFF
 

 마에다 준의 종단 유산. 개인적으로 손에 꼽는 감동물 사이 하나입니다. 작품의 주제는 '우정'이지만 섬세하고 따뜻하게 풀어나가기 그렇게 소년물과는 느낌이 다릅니다. 1기는 일반적인 형창 일상물의 전개로 캐릭터와 시청자와의 관계를 형성하고 Refrain에서 작품의 메인 스토리가 진행되면서 감동을 이끌어내는 구성을 가지고 있습니다. 전체적으로 전개가 느린 편이고 마에다 준 특유의 개그코드 그렇게 중간에 하차하는 경우가 많지만 1기에 걸쳐 쌓아 온 관계가 이환 감동의 베이스가 되기 왜냐하면 참고 보는 것을 권합니다.
 

 8/10 : 친구와 관련된 추억을 가지고 계신 분에게 추천한다.
 

## 8. 물드는 세계의 내일로부터
 구성 - 드라마, 청춘
 제작사 - P.A.WORKS
 

 오래 조율 실패, 상업적으로도 실패. 개인적으로 시로바코 이외에는 P.A.WORKS의 오리지널 애니메이션은 스토리의 완성도가 부족하다고 생각하는데 현 대표적인 예시로 보탬 작품을 듭니다. 특유의 색감을 살린 작화와 플레어를 강하게 넣은 연출은 훌륭했지만 이를 통해 전달하고 싶은 이야기를 전개하는 방식은 조잡하다고 느껴졌습니다. 그림체가 좋은 청춘물을 찾으시는 분에게 추천합니다.
 

 7/10 : 작화가 좋은 청춘물을 찾으시는 분에게 추천한다.
 

## 9. 바라카몬
 생김새 - 청춘, 일상, 코미디
 제작사 - 키네마 시트러스
 

 어른이 되어가는 이야기. 눈앞의 타인의 평가에 목매다는 청소년들에게 그것이 전부가 아님을 보여주는 작품입니다. 작품의 메인 소재인 서예에서 모티브를 얻은 듯한 연출과 깔끔한 작화, 아이들의 사투리까지 키네마 시트러스라는 제작사에 기대하는 완성도와 디테일을 보여줍니다. 특별히 OP의 가사는 작품의 주제를 백날 표현하고 있으니 필위 가사를 찾아보시기 바랍니다.
 

 9/10 : 20살 이하 청소년들에게 추천, 성장물을 좋아하시는 분에게 추천한다.
 

## 10. 바이올렛 에버가든
 생김새 - 드라마
 제작사 - 교토 애니메이션
 

 회도 괴물. 연속되는 그림으로 이루어진 애니메이션이라는 미디어에서 작화란 단순히 선이 많고 섬세한 것만이 좋은 것은 아니지만 금리 정도로 섬세한 작화는 지금까지 감상한 물품 중에서 최고라고 장담할 명맥 있습니다. 문제는 옴니버스식으로 진행되는 단편 에피소드들은 몰아가는 식으로 감동을 유도하고 메인 스토리인 바이올렛의 성장도 단편에 가려져 스토리텔링적인 면에서 아쉬운 모습을 보여줍니다. 작화와 연출은 최정상급 퀄리티를 보여주지만 특정 단편 에피소드 이외에는 전체적으로 스토리가 아쉬운 작품으로 정리할 핵 있겠습니다.
 

 9/10 : 쿄애니의 혼을 갈아 넣은 작화, 연출을 느껴보고 싶으신 분에게 추천한다.
 

## 11. 바쿠만
 생김새 - 코미디, 드라마, 청춘
 제작사 - J.C.STAFF
 

 만화가 도전기. 소년만화를 그리는 작품이기 왜냐하면 내용도 '우정, 노력, 승리' 클리셰를 따라갑니다. 실제로 만화가나 편집자를 모델로 테두리 특징 있는 캐릭터들을 통하여 본바탕 만화 업계를 엿볼 핵 있어서 좋았습니다. 담화 전개가 뻔하지만 중독성 있어 연달아서 송두리째 감상하였습니다. 일반적인 소년배틀물은 아니지만 소년물을 즐겨보시는 분이라면 추천하는 작품입니다.
 

 7/10 : 만화를 그리는 점프 만화, 중독성있는 점프 소년물을 즐기시는 분에게 추천한다.
 

## 12. 배를 엮다
 형체 - 드라마
 제작사 - ZEXCS
 

 새로운 분야에 흥미를 주는 작품. 글보다 면모 매체를 더 적잖이 소비하는 시대에 애니메이션이라는 자용 매체로 군자 고전적인 특징물 매체인 사전을 다루는 아이러니한 작품입니다. 하시 누구나 수정할 행복 있는 위키 사전을 사용하는 입장에서 특정 인물들이 한정된 오래도록 속에서 고심해서 애한 낱말 한단어 등재하는 것은 생소하면서도 매력적이었습니다. 새로운 것을 알아가는 즐거움을 느껴보고 싶으신 분에게 추천합니다.
 

 8/10 : 사건 간행이라는 새로운 분야를 알아가는 즐거움을 느껴보고 싶으신 분에게 추천한다.
 

## 13. 변태왕자와 웃지 않는 고양이
 형태 - 코미디, 학원, 청춘, 드라마
 제작사 - J.C.STAFF
 

 어른들의 사정이 반영된 청춘 러브 코미디. 사쿠라장과 아울러 제목과 말뜻 사이에 괴리가 있는 작품입니다. 브뤼케 캐릭터들의 갈등을 해결해 나가면서 성장하는 청춘 성장물이지만 다른 비슷한 작품에 비해서 시중 신이 많아 집중하기 힘듭니다. 전개도 애니메이션은 본격적인 스토리가 진행되기 전에 끝나서 뒷맛이 찝찝합니다. 전달하고 싶은 주제는 정작 묵직하고 깔끔한 칸토쿠 그림체는 마음에 들지만 이이 외의 나머지가 아쉬운 작품으로 정리할 핵 있겠습니다.
 

 7/10 : 서비스신이 많은 청춘 러브 코미디물을 찾으시는 분에게 추천한다.
 

## 14. 비스타즈(+ 2기)
 형상 - 청춘, 드라마
 제작사 - 오렌지
 

 초식동물과 육식동물이 공존하는 사회에서의 군상극. 육식이라는 행위가 만들어 내는 사회의 일그러짐 속에서 육식 또는 초식동물로 살아간다는 것에 대한 등장인물들의 시점을 비교하면서 보는 작품입니다. 육식과 초식이라는 좁혀질 운 없는 차이는 꼭꼭 본일 사회에서의 젠더, 인종, 경제적 수평 등의 차이를 표현하고 있는 듯합니다. 보석의 나라를 제작한 Orange 답게 3D작화는 이질감이 느껴지지 않고 1기, 2기 온통 기승전결이 확실한 완성도 높은 스토리를 보여줍니다. 사회적인 주제를 다루는 작품을 좋아하시는 분이라면 강력하게 추천하는 작품입니다.
 

 10/10 : 수인물을 싫어하셔도 묵직한 주제의 작품을 좋아하시는 분에게 추천한다.
 

## 15. 사랑은 비가 갠 뒤처럼
 형 - 청춘, 로맨스, 드라마
 제작사 - WIT STUDIO
 

 멈춰 서 있던 여고생과 중년의 습의 이야기. 먼저 말씀드릴 부분은 중년과 여고생의 치산 차이로 인한 거부감이 드는 상애 이야기는 아니라는 점입니다. 작품에서의 사랑은 상실을 극복하기 위해서 인생에서 단시 피할 곳을 의미하기 그러니까 일반적인 연모 장르에서 다루는 사랑과는 결을 달리합니다. 원작의 여러 은유적인 소품들이나 상황, 대화들이 애니메이션에서는 이미지로 표현되어 일층 효과적으로 주제를 전달하고 있습니다. 모든 스토리를 담아내지는 못했지만 나름 원작의 결말을 반영하여 깔끔하게 마무리지어 작품의 완성도도 높은 작품입니다.
 

 9/10 : 화려한 색감과 부드러운 광원 연출을 보여주는 섬세한 청춘 로맨스물.
 

## 16. 사쿠라장의 애완 그녀
 격식 - 로맨스, 드라마, 청춘, 학원
 제작사 - J.C.STAFF
 

 천재와 범인 사이의 갈등을 다루는 청춘물. 자작 노력하면 이루어진다는 단순한 주제의식과 다르게 내가 노력할 계절 다른 사람도 노력하기 왜냐하면 따라잡을 성명 있다는 보장은 없다는 것을 처음부터 마지막까지 일관되게 보여주는 점이 인상적이었다. 사쿠라장이라는 기숙사에서 등장인물들은 천재와 범인 사이에 존재하는 재능이라는 선천적인 차이를 어떻게 받아들일 것인가라는 질문의 답을 찾아간다. 조금은 억지스러운 흐름이 존재하기는 반대로 애니메이션이라는 매체의 강점을 살려 속뜻 있는 주제를 전달하는 작품이라고 생각한다.
 

 9/10 : 청춘물에 적합한 주제의식을 가지고 있는 작품, 제목에 속아 조교물로 착각하지 말 것
 

## 17. 소녀 종말 여행
 형태 - 포스트 아포칼립스, 일상, 드라마
 제작사 - 화이트 폭스
 

 포스트 아포칼립스 치유물. 포스트 아포칼립스 처지 특유의 분위기를 두 여자아이의 여행으로 무겁지 않게 표현하고 있습니다. 특유의 몽환적인 느낌은 키노의 여행과 비슷하나 주제가 사회적이기보다는 철학적인 면에 가까워 힐링물로 접근하기에도 나쁘지 않습니다. 여정 속에 깔려있는 철학적인 질문들은 시청자에게 무언가를 강하게 전달하기보다는 부드럽게 스며듭니다. 무거운 주제를 좋아하는 분에게도 가볍게 힐링하고 싶으신 분에게도 추천할 행운 있는 작품입니다.
 

 8/10 : 잔잔한 작품을 좋아하시는 분에게 추천한다. 느린 템포의 애니를 지루해하시는 분에게는 비추
 

## 18. 쇼와 겐로쿠 라쿠고 신쥬
 스타일 - 드라마
 제작사 - 스튜디오 딘
 

 스튜디오 딘도 하려면 할 행복 있다. 로쿠고(만담)이라는 독특한 소재를 다루는 작품입니다. 왜냐하면 원작과 달리 소리가 있는 애니메이션에서는 성우의 연기가 막 중요합니다. 득 작품은 이름만 들어도 알 정도의 레전드 급 베테랑 성우들을 기용하고 작화도 준수하게 뽑아 하나의 새로운 작품이 탄생하였습니다. 글쓴이도 페이트 스테이 나이트의 스튜딘이라는 이미지가 박혀있던 사람이기 그러니까 큰 기대를 단계 않았지만 느낌 후에는 숨겨진 명작으로 뽑는 작품입니다. 애니메이션에서 작품성을 기대하시는 분에게 추천합니다.
 

 9/10 : 성우의 연기가 애니메이션에서 얼마나 중요한지를 알 수 있는 작품, 진짜 숨겨진 명작
 

## 19. 시로바코
 장르 - 일상, 드라마
 제작사 - P.A.WORKS
 

 애니를 만드는 사람들이 애니메이션 제작을 애니 화한 작품. 애니메이션 제작에서 생기는 여러 상황들을 해결해 나가는 스토리입니다. 애니메이션 자체를 소재로 사용하고 있는 만치 감독, 연출, 애니메이터, 제작까지 업계의 모든 인물들이 총출동합니다. 모든 스태프들이 작품에 대한 이해도 높아서 그런지 모든 부분에서 작품에 대한 애정이 느껴집니다. 적절한 모에 요소와 깨알 같은 패러디는 덤. 애니메이션을 좋아하는 누구에게나 스스로 있게 추천할 명 있는 작품입니다.
 

 10/10 : 애니메이션을 좋아하시는 누구에게나 강력히 추천한다.
 

## 20. 암살교실 (+ 세컨드, 파이널 시즌)
 형태론적형태 - 학원, 코미디, 능력, 액션, 청춘
 제작사 - LERCHE
 

 워낙 학교에서 배워야 했던 것은 시고로 것이 아닐까... 학생들의 고민을 대하는 살선생의 자세는 일반적인 배움터 선생님을 강사로 보이게 합니다. 노치 점프 작품 특성상 스토리가 진행이 억지스럽고 액션신이 조금 아쉽긴 오히려 작품을 통해 전달하고자 하는 주제는 진심으로 다가왔습니다. 학교를 다녀본 모두에게 추천할 복 있는 작품으로 정리할 호운 있겠습니다.
 

 9/10 : 중, 고등학교를 다녀보신 누구에게나 추천한다.
 

## 21. 언덕길의 아폴론
 구성 - 청춘, 드라마, 음악, 로맨스
 제작사 - MAPPA, 데즈카 프로덕션

 

 60,70년대 촉각 청춘물. 60년대를 배경으로 하는 고등학생들의 풋풋한 객당 이야기입니다. 삼각관계 짝사랑 등 복잡해 보이는 관계성을 가지고 있지만 풋풋하게 풀어내어 막장전개로 흘러가지는 않습니다. 상업적인 작품을 발 않는 와타나베 감독답게 자극적이지 않고 담담한 연출을 보여주고 감성을 자극하는 재즈와 남주인공의 브로맨스 조합은 긴 여운을 남깁니다. 색다른 느낌의 청춘 로맨스물을 찾으시는 분에게 추천합니다.
 

 8/10 : 재즈라는 소재를 잘 살린 풋풋한 청춘물에 흥미가 있으신 분에게 추천한다.
 

## 22. 역시 내 청춘 러브코메디는 잘못됐다. (+ 속, 완)
 방식 - 학원, 코미디, 로맨스, 청춘
 제작사 - 브레인즈 베이스, feel.
 

 "나는 진짜를 원해"
 청춘이라는 주재를 유치하지만 아련하게 마무리 지은 학원 로맨스 코미디입니다. 주인공 힛키의 대사들이 중2스럽지만 그 속에 꽤 묵직한 의미가 들어 들어있습니다. 작가가 전하고자 하는 내용을 조금 더 섬세하게 풀어냈으면 잘 쓰인 청소년 소설이 되었을 것입니다. 1기의 작화는 당시 다른 작품과 비교해도 매우 처참한 수준이지만 2기 3기를 거치면서 몰라볼 정도로 좋아집니다.(마치 원작의 일러스트처럼...) 주인공도 스토리도 작화도 모두 시리즈를 거듭할수록 발전하는 애니메이션이라는 매체의 청춘에 해당하는 작품으로 정리할 수 있겠습니다.
 

 1기 7/10 : 작화에 신경 쓰시는 분에게 비추. 봇치에게 추천한다.
2기(속) 8/10 : 형태뿐인 친구에게 질리신 분에게 추천한다.
3기(완) 9/10 : 청춘이 지나간, 청춘을 겪고 있는 덕후 분에게 추천한다.
 

## 23. 영상연에는 손대지마
 폼 - 청춘, 드라마
 제작사 - 사이언스 SARU
 

 유아사 마사아키 독찰 작품. 지브리 작품에 나올법한 마을에 사는 3명의 여학생이 애니메이션을 제작하는 이야기입니다. 비슷한 소재의 시로바코와 비교하면 애니메이션이라는 영상이 만들어지는 과정을 더 전문적으로 다루고 있습니다. 감독의 자유롭고 비상업적인 성격과 원작의 내용이 시너지를 일으켜 영상에서 만든 사람의 흥이 느껴집니다. 애니메이션만이 가지는 상식에 구애받지 않는 영상을 추구하는 유아사 마사아키 감독의 작품에 입문하기 좋은 작품으로 정리할 요행 있겠습니다.
 

 9/10 : 핑퐁, 다다미 넉 장 반 세계일주의 감독 유아사 마사아키의 작품에 입문해 보고 싶으신 분에게 추천한다.
 

## 24. 우주보다 먼 곳
 요식 - 청춘, 드라마, 여행

 제작사 - 매드 하우스
 

 일상을 멈추고 어딘가 멀찌가니 떠나고 싶게 하는 작품. 오리지널 애니메이션이기에 쉬어가는 신고 한통 없이 13화 1쿨 안에서 기승전결 확실하게 결단 짓습니다. 함께 남극이라는 일상과는 거리가 먼 장소로 떠나면서 겪게 되는 고민들은 현실에서 친구와 함께 여행을 떠나면서 하는 그런 일상적인 고민들이 스토리에 담겨 있었습니다. 마음속에서는 생각하지만 말로는 꺼내지 않는 그러한 고민들을 모모 장면에서는 시원하게 아무개 장면에서는 씁쓸하게 바라보면서 감상하였습니다. 반복되는 삶을 살아가면서 품성 한편에 언젠가라고 생각하고 있다면 이윤 작품을 감상하고 용기를 신음 한 판정 내디뎌 보는 것은 어떨까 생각합니다.
 

 해상 자위대가 협력한 만큼 극우 미디어물이라는 꼬리표가 붙을 것은 예상하고 있었다. 하지만 내용 자체에서는 전쟁이나 과거사에 대해선 일절 건드리지 않는다. 감독은 극우세력으로부터 반일 감독으로 찍힌 감독이고 이 작품을 극찬한 평론가들 또한 좌익에 가까운 사람들이 많다. 결국은 9화에서 패전으로 인해 남극 영유권 행사에 불이익을 받았다는 뉘앙스 때문인데 이가 불합리하다는 내용은 아니다.(그렇다고 반성을 나타내지도 않지만 그렇다고 극우 미디어로 몰아가기에는 부족하다고 생각한다.)
 

 10/10 : 시로바코 이후로 가장 완성도 높은 오리지널 애니메이션, 애니메이션을 감상하는 것을 멈추고 멀리 떠나고 싶어 진다.
 

## 25. 울려라! 유포니엄 (+ 2기)
 정형 - 일상, 코미디, 드라마, 청춘
 제작사 - 교토 애니메이션
 

 케이온 현악기 버전 아님. 쿄애니, 음악, 학원이라는 장르에서 케이온과 같은 모에 일상물을 예상했지만 막상 열어보니 도무지 다른 작품이었습니다. 4쿨 TVA에  극장판까지 시리즈가 꽤 길어서 많은 등장인물들의 관계를 생략 가난히 섬세하게 다루고 있습니다. 현악기 작화는 물론이고 따뜻한 연출까지 비주얼적인 면의 만족도는 더없이 높았습니다. 기껏해야 개인적으로 높은 점수를 거소 않은 이유는 눈살을 찌푸리게 하는 캐릭터(아스카)가 존재하여 감상하는 내처 불편하였기 때문입니다. 소득 점만을 제외하면 허다히 완성도 높은 청춘 군상극이라고 할 목숨 있겠습니다.
 

 7/10 : 청춘 군상극을 좋아하시는 분에게 추천(다만 취향에 맞지 않는 캐릭터가 존재할 수 있음)
 

## 26. 원더 에그 프라이어리티
 외관 - 드라마, 청춘
 제작사 - CloverWorks
 

 애니메이션의 새로운 지평. 자살, 성폭력, 모조품 등 민감한 사회적인 이슈를 4명의 여학생들을 통해서 은유적으로 다루는 작품입니다. PD, 감독, 기품 디자인, 성우 전체 영리 작품이 데뷔작이고 각본가는 드라마에서는 유명하지만 애니메이션은 처음입니다. 반면에 이런 불안요소에도 불구하고 믿기지 않을 정도의 완성도를 보여줍니다. 특히 모든 화에서 고난도의 작화를 붕괴 가난히 완벽하게 보여주는 점에서 감탄했습니다. 글을 쓰고 있는 4월 1일 현재까지의 전개를 보았을 시기 2021년도 1분기 최고의 작품으로 평가합니다.
 

 스케줄 관리에 실패하여 12화 안에 결론 짓지 못하고 6월에 특별편으로 마무리될 예정입니다. 반면에 이러한 장르의 이야기가 편시 망가질 행우 있는 실수들(너무 많은 등장인물, 복선을 도무지 현실적으로 풀어내려 하는 점 등)을 족다리 않았기 때문에 주인공의 이야기를 빈빈히 마무리 지어 이번 분기의 최고의 작품이라는 타이틀을 가져갈 행우 있을 것으로 예상합니다.
 

 (임시)9/10 : 이전과는 다른 새로운 애니메이션을 경험해 보고 싶으신 분에게 추천한다.
 

 추신 : 스케줄의 문제로 아직 완결이 나지 않은 만큼 최종적인 작품의 완성도에 대해서 논할 수는 없으나 현재까지 보여준 모습에서 깔끔하게 마무리 지을 것을 기대함.
 

## 27. 은수저
 풍 - 학원, 청춘, 코미디
 제작사 - A-1 Pictures
 

 일상의 고민을 담은 작품. 농축업이라는 소재를 통하여 가축의 권리, 농업의 진실 등 일상에서 고민해 통기 않은 주제뿐만 아니라 청소년기의 진로에 대한 수심 같은 일반적인 고민들까지 여러 갈등을 다루고 있는 청춘물입니다. 삼농 고등학교에서의 일상은 진지하면서도 순수해서 바쁜 현실에 지친 사람들을 치유해 줍니다. 강철의 연금술자 작가의 작품답게 밀도감 있는 청춘물로 정리할 성명 있겠습니다.
 

 8/10 : 순수하고 따뜻한 작품을 찾으시는 분에게 추천한다
 

## 28. 작은 별의 플라네타리안
 폼 - SF, 드라마
 제작사 - david production
 

 짧고 굵은 감동물. 천체 관측과 로봇이라는 하 감성적인 소재를 다루는 작품입니다. 짧은 오래오래 안에서 주인 핵심이 되는 한도 줄의 대사와 같이 진한 감동을 전해줍니다. 음악도 콘테스트 원작에서 사용되었던 곡을 가져오고 웹으로 방영하여 저예산 작품임에도 퀄리티에서 부족함을 느끼지 못하였습니다. 시간적인 부담롱 없이 감동물을 보고 싶을 호기 추천합니다.
 

 8/10 : 극장용 애니를 감상하듯 짧게 볼 수 있는 치유물을 찾으시는 분에게 추천한다.
 

## 29. 저 너머의 아스트라
 형태 - SF, 판타지, 청춘
 제작사 - LERCHE
 

 반전 있는 SF 생존물. 먼 내일 다른 행성으로 가는 캠핑에서 조난당한 고등학생들이 집으로 귀환하는 여정을 더욱이 있습니다. 우주와 외계행성들의 묘사가 훌륭하고 풍경을 묘사할 동안 이외에는 시네마스코프 비율(21:9)을 사용하여 영화적으로 연출하고 있습니다. 이렇게만 설명하면 본격 SF물로 느껴질 행복 있으나 이야기의 본질은 여정 속에서 개개인 가지고 있는 고민을 해결하는 청춘 군상극에 가깝습니다. 진술 전체를 관통하는 복선도 존재하여 개연성도 뛰어나고 후반 마무리도 깔끔하여 SF로써도 청춘극으로써도 끔찍이 완성도 높은 작품으로 정리할 성명 있겠습니다.
 

 8/10 : 아름다운 천체 배경의 완성도 높은 청춘 군상극에 흥미가 있으신 분에게 추천한다.

 

## 30. 종말에 뭐하세요 바쁘세요 구해주실 수 있나요
 모양 - 드라마, 판타지, 포스트 아포칼립스
 제작사 - C2C, Satelight
 

 포스트 아포칼립스 철학 감동물. 초반과 기와 파트는 밝고 활기찬 내용이지만 시리어스 한도 장면에서는 히로인이 죽고 그 갭을 이용하여 감동을 유도하는 작품입니다. 비슷한 구성의 플라스틱 메모리즈와는 달리 메인 히로인 이외의 히로인들을 테두리 명씩 소모하면서 감정을 유도합니다. 이러한 석리 왜냐하면 스토리에서 감정이 소모될 뿐 여운이 남는 감동을 자아내지는 못하고 있습니다. 연출 유난스레 내종 클라이맥스 연출은 지지리 아름답고 작화도 꽤나 안정적이지만 스토리가 이를 받쳐주지 못한다는 느낌을 지울 행운 없었습니다. 몰아가는 식의 감동물을 좋아하시는 분에게 추천합니다.
 

 7/10 : 몰아가는 느낌의 감동물을 좋아하시는 분에게 추천한다.
 

## 31. 청춘 돼지는 바니걸 선배의 꿈을 꾸지 않는다
 구성 - 드라마, 청춘, 로맨스
 제작사 - Clover Works
 

 사춘기의 고민을 서브컬처 방식으로 표현한 작품. 각 히로인들은 우리가 고민할 생명 있는 고민을 가지고 있고 작품은 사춘기 증후군이라는 초자연적인 현상을 통해서 히로인들의 고민을 눈에 보이는 현상으로 표현하고 있습니다. 소유자 마음에 드는 점은 주공 사쿠타가 다른 러브 코미디와 다른 게 둔감하지 않고 외려 적극적이기에 초반 마석 라인이 정해지고 이후 히로인들의 고민을 해결하는데 초점이 맞춰져 있다는 점입니다. 청춘 러브 코미디를 좋아한다면 적극적으로 추천합니다.
 

 8/10 : 옳이 만들어진 청춘물을 찾으시는 분에게 추천한다.(사쿠라장이 마음에 드신 분이면 특히)

 

## 32. 충사 (+ 속장)
 격식 - 드라마, 미스터리
 제작사 - 아트랜드
 

 몽환적인 분위기의 치유물. 눈에 보이지 않는 생명체에 가까운 존재인 무시(벌레)에 관련된 사람들의 이야기를 충사 긴코의 시점에서 풀어나가고 있습니다. 옴니버스 형식으로 진행되는 단편 에피소드들은 억지스러움 없이 감동을 줍니다. 원작 특유의 색감을 살린 작화에 잔잔한 음악이 더해져 신비롭고 몽환적인 느낌을 전달하고 있습니다. 모노노케 히메를 좋아하시는 분이라면 유익 작품 더더군다나 취향에 맞으실 겁니다.
 

 9/10 : 몽환적이고 기묘한 작품을 좋아하시는 분에게 추천한다.

 

## 33. 카우보이 비밥
 문법적형태 - SF, 드라마, 액션
 제작사 - 선라이즈
 

 어른들을 위한 애니메이션. 작품 전체에 걸쳐서 주제의식을 내절로 언급하지 않고 캐릭터들의 이야기를 통하여 은유적으로 보여줍니다. 캐릭터의 내면을 제출물로 표현하지 않고 관찰자의 시점에서만 보여주기 때문에 강요당하는 오감 궁핍히 자연스럽게 느끼고 시청자가 각자의 결말을 낼 호운 있도록 합니다. 시고로 스토리텔링에 적합한 누아르, 하드보일드적인 분위기에 재즈와 블루스를 기반으로 경계 음악, 디테일한 표현까지 구현되어 있는 배경작화 등 다른 작품들과는 대단히 차별화되는 요소가 많습니다. 묵직하게 깔리는 음악에 몸을 맡기고 커피를 한잔 마시며 차차로 즐기는 작품으로 정리할 핵 있겠습니다.
 

 10/10 : 그루브에 몸을 맡기고 커피를 마시며 즐길 작품을 찾으시는 분에게 추천한다.(어른 한정)
 

## 34. 크게 휘두르며
 정형 - 스포츠, 드라마, 청춘
 제작사 - A-1 Pictures
 

 스포츠물에 드라마를 더하다. 다이아몬드의 에이스보다는 더욱 통상 고교 야구에 가까운 모습을 보여주는 작품입니다. 주인공 보정이 사뭇 약하지 않은가라는 생각이 교외 정도로 주인공이 약한 존재감을 보여주는 대리 다른 선수들의 심리와 치밀한 작전으로 이야기를 풀어나갑니다. 특유의 열혈 분위기도 약해서 성장 드라마물로써 즐길 행우 있습니다. 대용 신장 속도가 대단히 느리기 그리하여 일반적인 스포츠물을 기대하고 보면 지루할 운명 있습니다. 열혈, 근성만 외치는 작품에 질리신 분에게 추천합니다.
 

 8/10 : 주인공의 성장에 초점이 맞춰져 있는 스포츠 군상극, 일반적인 스포츠물을 즐기시는 분에게 비추한다.
 

## 35. 클라나드 (+ AFTER STORY)
 외관 - 드라마, 로맨스, 학원, 판타지
 제작사 - 교토 애니메이션
 

 개인적으로 치유물 원톱. key사의 대표적인 미연시 클라나드를 애니메이션화 벽 작품입니다. 1기는 월자 히로인들의 스토리가 진행되고 2기 에프터 스토리에서 메인 주제인 가족에 대한 스토리가 진행됩니다. 초반에는 단순한 학원물로 느껴질 요행 있으나 후반 이러한 일상들이 모여 감동을 자아냅니다. 원작 원화에 따라 눈을 아주 크게 그려서 약간 부담스러울 운명 있으나 익숙해지면 뜻밖에 괜찮습니다. 작화가 올드하긴 도리어 무너지지는 않고 연출도 뛰어납니다. 감동을 자아내는 치유물을 좋아하시는 분에게 필위 추천합니다.
 

 10/10 : 몰아가는 감동이 아닌 자연스러운 감동을 느껴보고 싶으신 분에게 추천한다.
 

## 36. 키노의 여행 -the beautiful world-
 게슈탈트 - 드라마, 판타지
 제작사 - LERCHE
 

 라노벨계의 불후의 명작. 현대 사회의 일면을 하나씩 반영하고 있는 나라들을 3일간 여행하는 옴니버스식의 단편집입니다. 전체적으로 묘사가 직설적이고 잔혹하지만 노형 속에 몹시 무거운 주제들을 담고 있습니다. 판타지라는 장르에서는 일반적인 상식으로는 이해할 운 없는 요소들이 허용되기 그리하여 무겁기만 할 생령 있는 주제를 이러한 허용을 이용하여 효과적으로 전달하고 있습니다. 작가의 혐한 인식이 사건 되지 않는다면 적극 추천하는 작품입니다.
 

 8/10 : 무거운 주제가  담겨 있는 작품을 찾으시는 분에게 추천한다.

 

## 37. 토끼 드롭스
 폼 - 드라마, 일상
 제작사 - Production I.G
 

 육아 일기. 노총각인 다이키치가 외할아버지의 숨겨둔 딸을 맡게 되면서 벌어지는 일상을 게다가 있는 작품입니다. 애니에서는 린이 아직 어릴 때만을 다루기 그리하여 사춘기와 같은 복잡 미묘한 고민을 다루지는 않습니다. 서로를 알아가면서 가족으로 받아들이는 과정을 통해 자연스럽게 감동을 자아냅니다. 눈물을 유도하는 식의 감동물에 지치신 분에게 추천합니다.
 

 8/10 : 감정을 몰아가는 식의 감동물에 지치신 분에게 추천한다.
 

## 38. 토라도라
 모습 - 로맨스, 학원, 청춘
 제작사 - J.C.STAFF
 

 정교 애니의 정석. 5명의 등장인물들 간에 얽혀 있는 관계를 풀어가면서 성장하는 청춘 로맨스물입니다. 호리에 유이, 쿠기밍 같은 유명 성우진과 2000년대 잘 나가던 제작사 J.C의 작품이라서 작품의 퀄리티는 훌륭합니다. 특별히 감정이 폭발할 때의 작화와 연출 또한 처음과 끝을 이어주는 수미상관 구성을 통해서 감상한 이후에도 진한 여운이 남습니다. 애니메이션을 좋아한다면 유달리 연애 장르를 좋아한다면 기연히 봐야 하는 작품으로 정리할 복 있겠습니다.

 

 이 세상에 누구 하나 본 적도 없는 것이 있다. 그것은 다정하고도 무척이나 달콤하다. 아마 눈에 보인다면 누구나 그것을 가지고 싶어 할 것이다. 그렇기에 아무도 그것을 본 적이 없다. 그리 쉽게 손에 넣지 못하도록 세상은 그것을 숨겨왔다. 하지만, 언젠가는 누군가가 발견할 거다. 손에 넣을 단 한 사람이 분명 그것을 발견해낼 것이다 그렇게 되어있다...
 

 10/10 : 로맨스 애니의 정점, 후유증이 심하게 올 가능성 농후
 

## 39. 플라스틱 메모리즈
 형 - 드라마, 로맨스, SF
 제작사 - 동화공방

 

 동화공방의 SF풍 로맨스. 어디까지나 사랑 작품이기 그러니까 인간과 기계의 경계와 같은 SF적인 고민을 찬찬히 있게 다루지는 않습니다. 결말을 초반부터 예측할 성명 있기 그렇게 1쿨13화 안에서 결말을 추론하는데 시간을 낭비하지 않고 밀도 있게 관계를 쌓아나가 여운이 남는 마무리를 보여줍니다. 작화도 안정적이고 잔잔한 연출에서 둘의 감정을 섬세하게 다루고 있음을 느낄 목숨 있었습니다. 뻔하지만 슬픈 로맨스를 선호하시는 분에게 추천합니다.
 

 8/10 : SF풍 로맨스에 흥미를 느끼시는 분에게 추천한다.(본격 SF를 기대하면 비추)

 

## 40. 핑퐁
 형태 - 스포츠, 청춘
 제작사 - 타츠노코 프로덕션
 

 '깔끔한 작화 = 좋은 작화'라는 고정관념을 깬 작품. 유아사 마사아키 감독의 풍부한 표현력을 느낄 길운 있는 작품입니다. 구불구불하고 거친 원적 덕분에 카메라 앵글을 과격하게 잡을 요체 있었고 빠른 시선의 움직임이 속도감을 만들어내는 연출을 사용하고 있습니다. 이러한 연출을 통해서 유아사 감독이 추구하는 상상력 넘치는 움직임을 경험할 고갱이 있습니다. 상업성과 예술성을 적절하게 타협하여 대중에게도 좋은 평가를 받는 작품이니 금리 작품을 통해서 애니메이션을 보는 안목을 높여보는 것은 어떨까...
 

 9/10 : 상업 애니만을 즐기시는 분에게는 비추, 예술성과 작품성이 좋다고 평가되는 애니가 어떤 것인지 궁금하신 분에게 추천한다.
 

## 41. 하트 커넥트
 방식 - 청춘, 로맨스, 학원
 제작사 - 실버 링크
 

 성우 논란에 여러모로 묻혀버린 작품. 청춘 돼지와 함께 판타지적인 도리 속에서 등장인물들이 가지고 있는 감성적 갈등을 풀어나가는 이야기입니다. 각각의 히로인들의 갈등을 옴니버스 식으로 풀어나간 청춘돼지와는 달리 유익 작품은 모든 등장인물이 나란히 사건을 경험하고 양서 영향을 주고받으며 사고가 변화합니다. 복잡한 감정들을 차근차근 하나씩 풀어나가는 만족감이 있고 버데기 사건마다 기승전결이 확실하여 끊어서 보기도 좋았습니다. 성우 논란 왜냐하면 후속작은 기대할 수 없지만 도리어 참으로 만족한 청춘 로맨스물입니다.
 

 8/10 : 성우 논란으로 덮어버리기에는 너무 아까운 청춘 로맨스물
 

## 42. 허니와 클로버
 장르 - 청춘, 로맨스, 드라마
 제작사 - J.C.STAFF
 

 어른들을 위한 청춘물. 성인이 되어가는 통로 속에서 하는 고민들과 섬세한 정교 등 청춘 자네 자체로 이루어진 작품입니다. 2000년대 초반 스타일의 곡들이 배경에 깔리고 명대사들이 심금을 울립니다. 파스텔 톤의 작화와 부드러운 연출은 우미노 치카의 작품을 자연스럽게 표현하고 있습니다. 청춘이 지나가버린 분에게 아련한 감정이 들게 해주는 더구나 시방 겪고 있는 분에게는 등을 떠밀어주는 작품으로 정리할 행운 있겠습니다.
 

 10/10 : 가벼운 마음으로 추천하고 싶지 않은 나의 인생작
 

## 43. Angel Beats !
 형식 - 학원, 액션, 드라마, 코미디
 제작사 - P.A.WORKS
 

 고전 치유물. 감동을 느낄 수밖에 없는 구도로 이끌어 가는 제품 새중간 하나입니다. 약 6쿨에 걸쳐 방영해야 할 정도로 방대한 시나리오를 1쿨(13화) 안에 압축하였기 그리하여 개연성이 전연 부족합니다. 반면 P.A만의 작화와 연출의 시발점이라고 할 수 있는 작품인 점 또한 주옥같은 OST들이 이런즉 단점을 커버해주기 그렇게 극히 인상에 남는 작품입니다. 작품성은 아쉽지만 많은 사람의 기억에 남을 복 있는 작품으로 정리할 삶 있겠습니다.
 

 8/10 : 의도적으로 감정을 흔들어 놓는 스토리를 좋아하시는 분에게 추천한다.
 

## 44. ReLIFE
 형식 - 청춘, 학원, 로맨스
 제작사 - TMS 엔터테인먼트
 

 적은 예산이 아쉬운 청춘물. 본래는 웹 애니메이션으로 제작할 예정이었던 작품이라서 예산이 넉넉하지 않았고 때문에 퀄리티는 대단히 떨어집니다. 겨우 깔끔한 연출과 담담한 애모 라인 덕분에 작품의 주제를 어느 전경 효과적으로 전달하고 있습니다. 개인적으로 이금 작품은 원작을 권하지만 애니메이션도 원작의 느낌을 어느정도 살리고 있어 나쁘지 않은 작품으로 정리할 생목숨 있겠습니다.
 

 7/10 : 청춘 로맨스 작품을 좋아하신다면 추천한다.(다만 저예산 작화...)
 

## 45. 3월의 라이온
 맨드리 - 드라마, 청춘
 제작사 - 샤프트
 

 인생에 영향을 줄 행운 있는 작품. 허니와 클로버가 어른들의 청춘물이었다면 치아 작품은 소년, 소녀의 성장을 다루고 있습니다. 주인공의 내면적인 향상 이야기와 아울러 가 인물들의 개별 에피소드가 아울러 진행되는 군상극의 형태를 띠고 있습니다. 원작의 수채화 느낌의 색상을 살리면서 샤프트의 섬세한 작화로 표현하고 있고 OP, ED에 허니와 클로버를 담당했던 YUKI뿐만 아니라 요네즈 켄시 등 유명 뮤지션들이 참가하고 있습니다. 샤프트 행사 연출을 싫어하시는 분도 1기에 걸쳐 차츰차츰 작품과 동화되어 2기에서 빛을 바라니 선입견을 가지지 말고 감상해 주셨으면 합니다.
 

 10/10 : 상처를 떠안고 있는 소년, 소녀에게 이 추천글을 바칩니다.
 

## 46. 4월은 너의 거짓말
 폼 - 청춘, 드라마, 로맨스
 제작사 - A-1 Pictures

 

 추천할 행운 있는 감동물. 극 초반부터 결말의 형태가 예상되고 내나 감동을 위해서 분위기를 몰아가는 전형적인 감동물입니다. 오히려 제조품 전체에 걸쳐서 주인공과 히로인의 관계를 중심으로 차근차근 쌓아나가기 때문에 거부감이 드는 감동은 아니었습니다. 재주 인물과의 에피소드도 같이 진행되지만 어디까지 메인 Boy Meets Girl입니다. 애니메이션의 강점을 살릴 행운 있는 음악이라는 소재와 하이라이트 장면에서의 고난도 연주 작화까지 담론 이외의 부분도 만족스러웠습니다. 감동물에 입문하시는 분에게 추천하는 작품입니다.
 

 9/10 : 후유증이 남는 감동물을 좋아하시는 분에게 추천한다.
 → 개인적으로 청춘물을 선호하여 높은 점수를 준 작품이 매우 있는 장르입니다.
→ 대부분 감동적인 애니라던가 후유증이 심한 애니로 표현되는 장르입니다.
 

 #나츠메 우인장 #바라카몬 #사랑은 비가 갠 뒤처럼 #사쿠라장의 애완 그녀 #암살교실 #우주보다 먼 장소 #울려라! 유포니엄 #종말에 뭐하세요 바쁘세요 구해주실 수 있나요 #카우보이 비밥 #비스타즈 #플라스틱 메모리즈 #Angel Beats ! #4월은 너의 거짓말 #3월의 라이온
