---
layout: post
title: "[101] [ㄱ-ㄴ]여배우-모델-가수(20191027)★★★"
toc: true
---



 ['차단용 토끼'의 녹음기록 파일형식 117]-(20220530)-[비밀결사체들과 재벌카르텔의 '비밀스러운 여성들 명단']
 -----------------------------------------------------------------------------------------------------------------------------------------------------------
 

 

 

 

 

 

 

 -----------------------------------------------------------------------------------------------------------------------------------------------------------
 (기록: 2019.10.31/목요일/PM 05:10)
 

 [남작 작위]
 강민경(1990년)/한국의 여가수, 배우
 이름: 강민경(姜珉耿)
본관: 진주 강씨
종교: 개신교
출생: 1990년 8월 3일 (28세), 경기도 파주시
국적: 대한민국
신체: 167cm, 50kg, O형
가족: 부모님, 오빠(1987년생), 남동생(1999년생)
학력: 금촌초등학교
냉천초등학교
저동중학교
정발고등학교
세화여자고등학교
경희대학교 포스트모던음악학과
 소속 그룹: 다비치
소속사: 스톤 뮤직 엔터테인먼트
데뷔: 2008년 1월 28일 다비치 1집 'Amaranth'
포지션: 비주얼담당, 리드보컬
 [남작 작위]
 강민아(1997년)/한국의 여배우
 이름: 강민아 (姜旻兒)
출생: 1997년 3월 20일(21세), 서울특별시
신체: 160cm, 40kg, AB형
학력: 오금중학교 (졸업)
서울방송고등학교 방송연예과 (졸업)
 가족: 부모님, 쌍둥이 오빠, 반려묘 고래,새우,상어
데뷔: 2009년 영화 '바다에서'
소속사: 이매진아시아
드라마: (2017년)모두의 연애(주연: 강민아)
 [준백작 작위]◇
 강소라(1990년)/탤런트, 영화배우
 출생: 1990년 2월 18일 (만 28세)
소속사: 플럼액터스
학력: 동국대학교 연극학과
데뷔: 009년 영화 '4교시 추리영역'
 [준남작 작위]
 강은비/한국의 배우
 본명: 주미진
국적: 대한민국
출생: 1986년 4월 15일 (32세)/서울특별시
성별: 여성
신체: 163cm, 47kg
가족: 부모님, 남동생(1993년생)
직업: 배우, 가수, 모델
데뷔: 2005년 '몽정기2'
학력: 서울왕북초등학교
숙명여자중학교
안양예술고등학교(전학)
청담고등학교
서울예술대학교 방송연예과

## 혈액형: O형
 [기사 작위]
 강정현(KANG JEONG HYEON)/2018년 미스코리아 후보자
 지역: 광주전남
나이: 22세
출신: 정보호남대학교 항공서비스학과 4학년 재학
장래희망: 승무원
취미/특기: 영화감상 , 특기 - 요리
신체정보: 174cm/54.7kg/36 - 25 - 35
 [준자작 작위]
 강초원 (Cho Won [홍대 필라테스](https://scarfdraconian.com/life/post-00043.html) Kang)/한국의 모델
 출생: 1991년 9월 9일
소속사: 에이전시가르텐
경력: 2016 잡지 W KOREA, SINGLES, BLING, DAZED&CONFUSED 모델
2016 F/W 서울패션위크 SJYP 모델
2016 S/S 서울패션위크 JARRET 모델
 [백작 작위]★
 강한나/대한민국 배우
 출생: 1989년 1월 30일(29세), 서울특별시 중랑구
신체: 168cm, O형
학력: 중앙대학교 연극영화학
중앙대학교 대학원 연극영화학(석사과정)
 가족: 부모님, 언니 2명
국적: 대한민국
대리인: 판타지오
 [준남작 작위]
 김가은(1989년 1월)/한국의 배우
 이름: 김가은(金佳恩)
출생: 1989년 1월 8일 (30세), 서울특별시
가족: 부모님, 뭉찌, 심바
신체: 164cm, 47kg
성좌/지지: 염소자리/용띠
학력: 국민대학교 연극영화학
데뷔: 2009년 SBS 11기 공채 탤런트
소속: 뽀빠이엔터테인먼트
 [남작 작위]
 김계령/2018년 미스코리아 미/2018년 미스코리아 인천 진
 나이: 22세
학력: 서강대학교 경영학과
장래희망: 아나운서, 재무분석가
취미/특기: 발레, 첼로 연주
신체정보: 172.4cm/50.8kg
경력: 2018년 미스코리아 미
2018년 미스코리아 인천 진(우승)
 [준남작 작위]
 김규리/2015년 빈틈 경남 진
 나이: 26세
경력: 2015년 과실 경남 진
 [기사 작위]
 김규리(1996년)/2018년 미스코리아 부산-울산 선
 출생: 1996년 1월 9일, 부산광역시
학력: 부산대학교 심리학과
신체: 177.4cm/54kg/34-24-35
경력: 2018년 미스코리아 부산-울산 선
2018년 미스코리아 2차 통과자
 [기사 작위]
 김규리/2017년 미스코리아 투쟁 선
 소속: 에스팀 소속 패션 모델
경력: 2017년 미스코리아 경기 선
 [준남작 작위]
 김고은(Kim Go Eun)/배우
 출생: 1991년 7월 2일(27세), 서울특별시
국적: 대한민국
신체: 167cm, 48kg, B형
학력: 계원예술고등학교
한국예술종합학교 연극원
 가족: 부모님, 오빠(1990년생)
종교: 개신교
데뷔: 2012년 영화 '은교'
소속사: BH엔터테인먼트
 [남작 작위]
 김나영(1989년)/2014년 미스코리아 USA 미(참가번호 32번)
 설명: 2014 미스코리아 USA 미. USA 대회에서는 한국일보에 입상했으나 본선에서는 미로 출전하였다
 참가번호: 32
생년월일: 1989년 9월 25일
신체사항: 167.3cm, 48.2kg, 34-24-34
학교: University of Alberta 경제학과
취미: 음악감상, 마라톤
특기: 태권도, 하키
장래희망: UN 근무 및 봉사
경력: 2014년 미스코리아 USA 미(참가번호 32번)
 [준남작 작위]
 김나영/2018년 미스코리아 경북 미
 나이: 24세
신체: 175.1cm/56.9kg
학력: 대경대학교 모델과 2학년
특기: 필라테스
장래: 희망모델
경력: 2018년 미스코리아 경북 미
 [기사 작위]
 김나희/한국의 코미디언
 출생: 1988년 4월 16일 (만 30세)
신체: 168cm
소속사: 타조엔터테인먼트
데뷔: 2013년 KBS 28기 공채 코미디언
학력: 경기대학교 (–2011년)
영화: 우는 남자
 [준자작 작위]
 김다예(1990년)/ 한국의 여배우
 출생: 1990년 9월 24일 (28세), 서울특별시
신체: 163cm, O형
학력: 서울예술대학교 연기과
데뷔: 2012년 CF '맥스' 모델
소속사: 싸이더스HQ
영화: (2015년)순수의 시대(배역: 경순공주)
(2018년)SBS 복수가 돌아왔다(배역: 계소라)
 [준남작 작위]
 김도연/2014년 미스코리아 운행 미
 참가번호: 5
생년월일: 1990년 5월11일
신체사항: 172.2cm 53.1kg 34-23-36
학교: 배화여자대학교 중국어통번역과
취미: 요리, 영화감상
특기: 수영, 요가
장래희망: 방송인
 [기사 작위]
 김려은(KIM RYEO EUN)/2017년 2차 통과자
 지역: 대구
나이: 만 27세
신체: 172.6cm / 50kg / 34-24-36
출신: 정보경북대학교 대학원
전공: 신문방송학과
취미: 헬스 / 사진찍기
특기: 캘리그라피 / 포토포즈
 [남작 작위]
 김로사/한국의 모델
 나이: 24세(만 22세) 게자리 쥐띠
출생: 1996년 7월 18일
소속사: 에스팀
신체: 176cm
 [준남작 작위]
 김미정/2018년 미스코리아 경북 참가(참가번호 11번)
 나이: 20
학력: 연성대학교 항공서비스학과 2학년
특기: 이미지컨설팅, 태권도
장래: 희망뉴스앵커
경력: 2018년 미스코리아 경북 참가(참가번호 11번)
 [기사 작위]
 김민지/2016년 병 대구 참가자
 잘못 대구 참가번호: 4번
나이: 만 22세
학력: 대경대학교 모델과 2학년
특기: 한국무용
장래희망: 모델
경력: 2016년 양 대구 참가자
 [남작 작위]
 김민정/한국의 모델/2016년 미스코리아 미
 설명: 2016 실조 경북 라인 김나경과는 쌍생아 자매다.
 이름: 김민정
미스 경북 참가번호: 9번
나이: 만24세
학력: 계명대학교 미국학과 3학년
특기: 요리하기
장래희망: 패션MD
 [기사 작위]
 김별이(BYEOLI KIM)/2018년 2차 미스코리아 통과자
 지역: 충북
나이: 23
학력: 광주여자대학교 항공서비스학과
장래희망: 교수, 장학재단 설립자
취미/특기: 독서, 홈트레이닝, 요거트 만들기/음악감상/요가, 댄스
신체정보: 174.7cm / 51.2kg
 [남작 작위]
 김보라(1995년)/한국의 배우
 이름: 김보라
출생: 1995년 9월 28일 (23세), 서울특별시
신체: 161cm, A형
학력: 소양초등학교
계양중학교
인천예일고등학교
인하대학교 예술체육학부 연극영화학과
 가족: 부모님, 큰언니(1990년생), 작은언니(1993년생)
데뷔: 2005년 KBS2 희곡 '웨딩'
소속사: iHQ
 [남작 작위]
 김보라/2016 슈퍼모델 선발대회
 명칭 : 김보라
나이 : 27세
키 : 166.3cm
경력: (2016년)SBS 슈퍼모델 선발대회 본선 잡도리 진출자
유튜브: https://www.youtube.com/channel/UCuYtwhBPiVI5UiZgT3GEdAw/featured
 [경력]
 카스 맥주광고 캔서트 편
BBQ치킨 이종석,수지편수지 부분대역모델
G마켓히어로 편
다이어트약칼로커트–이보영 몸매대역모델&
(칼로커트2016-서브 모델)
 [준남작 작위]
 김보미/한국의 배우
 이름: 김보미(金甫美)
출생: 1987년 5월 15일, 경기도 수원시
가족: 부모님, 2녀 한가운데 장녀
학력: 세종대학교 무용학과
데뷔: 2008년 SBS 극 '바람의 화원'
소속: 플라이업 엔터테인먼트
 [준남작 작위]
 김사랑/탤런트, 영화배우
 설명: 김사랑은 대한민국의 미스코리아 신분 배우이다.
출생: 1978년 1월 12일 (40세), 대구광역시
방송: 사랑하는 은동아, 시크릿 가든, 이전 죽일 놈의 사랑, 천년지애, 도쿄 여우비, 왕과 나, 정, 미나, Affection
 형제자매: 김대혜
학력: 용인대학교 대학원 국악학과, 광영여자고등학교
신체: 173cm, 49kg/O형
가족: 제매 김대혜
데뷔: 2000년 제44회 미스코리아 선발대회
경력: 2018.10 초록우산 어린이재단 홍보대사
 [준남작 작위]
 김서연/한국의 모델/2014년 미스코리아 당선자
 설명: 2014 실족 한가운데 진으로 뽑혀 미스코리아 본선에 진출해 이놈 타격 진이 되었다. 2015 허므로이 유니버스에 대한민국 대표로 출전했다. 아버지가 부산 부지 대학의 교수다.
 이름: 김서연
출생: 1992년 5월 19일 (26살), 서울특별시
학력: 이화여자대학교 경영학과
신체: 172.8cm/51.4kg/33-24-35
경력: 2014년 미스코리아 한복판 진
2014년 미스코리아 당선자
2015년 위례 유니버스 코리아
 [기사 작위]
 김서원/2018년 미스코리아 대구 선
 나이: 22
학력: 백석예술대학교 항공서비스과 졸업
특기: 연기,다양한 스포츠,무용
장래희망: 모델겸 배우
경력: 2018년 미스코리아 대구 선
 [준남작 작위]
 김설희/한국의 모델
 출생: 1998년 2월 21일
신체: 178cm
소속사: YG케이플러스
경력: 헤라서울패션위크 자렛, 노케, 오디너리피플, 노앙, KYE, 로맨시크, LIE, 송지오, A.Bell 패션쇼 모델
 [준자작 작위]
 김수민(1994년)/한국의 모델/2018년 미스코리아 진
 설명: KBS 2TV 해피투게더 8월 2일 방송분에 출연하였고 같은 타격 10월에는 KBS 1TV 6시 내고향에서 강남과 아울러 출연하였다.
 출생: 1994년 10월 1일 (24세)
학력: 디킨슨 분과대학 국제경영학과
신체: 173.4cm, 58.9kg, 35-25-38
경력: 2018년 미스코리아 진(우승)
 [기사 작위]
 김수현(KIM SU HYUN)/2018년 미스코리아 후보자
 지역: 서울
나이: 22세
출신정보: 홍익대학교 법학과(공법전공) 3학년 휴학
장래희망: 판사
취미/특기: 미술관 관람, 미술작품감상, 여행, 요가, 필라테스
특기: 수학, 스키, 와이어공예, 요가, 필라테스
신체정보: 172.9cm/50.8kg/34 - 24 - 35
 [준자작 작위]
 김소은(1989년)/한국의 배우
 이름: 김소은(金素恩) / Kim So-eun
출생: 1989년 9월 6일 (29세), 경기도 남양주시 퇴계원면
신체: 160cm O형
학력: 금교초등학교
도농중학교
수택고등학교
중앙대학교 연극영화학과 (휴학중)
 가족: 부모님, 여동생(1995년생)
취미: 복싱, 사격
데뷔: 2005년 MBC 희곡 '자매바다'
소속사: 윌엔터테인먼트
 [준남작 작위]
 김소은(KIM SO EUN)/2017년 미스코리아 2차 통과자
 지역: 경남
나이: 만 24세
신체: 170.5cm / 49.1kg / 35-24-36
출신: 부산대학교
전공: 한국음악학과
취미: 스쿠버다이빙 / 여행
특기: 대금연주 / 장구춤
장래희망: 한국음악학과 교수
경력: 2017년 미스코리아 2차 통과자
 [기사 작위]
 김소윤/2016년 에러 대구 참가자
 참가번호: 12번
나이: 만 24세
학력: 영진전문대학교 국제관광계열 졸
특기: 요리,스피닝
장래희망: 객실승무원
경력: 2016년 에러 대구 참가자
 [기사 작위]
 김소희/2014년 미스코리아 수도 미
 설명: 2014 미스코리아 수도 대회에 참가했으나 진, 선, 미 입상에는 실패, 이후 열린 패자부활전에서 입상하여 본시 참가했던 서울 지역의 미 자격으로 본선에 진출했다.
 생년월일: 1989년 10월 6일
신체사항: 171.8cm, 51kg, 35-24-36
출생지역: 대한민국
학력: 숙명여자대학교 무용과
가족관계: 부모님, 언니 김민주
 [준자작 작위]
 김소현(1999년)/한국의 배우
 이름: 김소현(金所泫)/Kim So-hyun
출생: 1999년 6월 4일 (19세), 오스트레일리아
국적: 한국
본관: 김해 김씨(金海 金氏)
신체: 165cm, 45kg, O형, 발사이즈 240mm
학력: 회룡초등학교 (전학)
용인토월초등학교 (졸업)
용인문정중학교 (졸업)
고등학교 졸업학력 검정고시 (합격)
한양대학교 연극영화학과 (재학)
 가족: 어머니, 남동생(2000년생), 반려견 몽숙이
데뷔: 2008년 KBS 극 '전설의 요람지 - 아가야 청산가자'
소속사: E&T Story By PLAN A
 [남작 작위]
 김소혜/한국의 배우/前 I.O.I
 이름: 김소혜 (金素慧)/Kim So-Hye
출생: 1999년 7월 19일 (19세), 서울특별시 강남구 (삼성서울병원)
국적: 대한민국
본관: 선산 김씨(善山 金氏)
신체: 165cm, 48kg(+2.1), A형
학력: 서울영희초등학교 (졸업)
숙명여자중학교 (졸업)
경기여자고등학교 (졸업)
 소속사: S&P엔터테인먼트
소속 그룹: I.O.I
포지션: 서브보컬
데뷔: 2016년 I.O.I 땅딸보 앨범 1집 'Chrysalis'
영화: (2019년)만월
드라마: (2019년)최고의 치킨: 서보아 배역
가족: 아버지 김호원(1974년생), 모씨 이승란, 남동생 김윤수 (2001년생)
종교: 개신교
 [기사 작위]
 김영은/2018년 미스코리아 경북 참가(참가번호 10번)
 나이: 24
학력: 인하대학교 예술체육학부 4학년
특기: 무대디자인, 동화구연, 수영
장래희망: 문화예술교육사
경력: 2018년 미스코리아 경북 참가(참가번호 10번)
 [남작 작위]
 김예림(1994년)/한국의 여가수
 이름: 김예림(金藝琳, Kim Yae-lim)/Lim Kim
출생: 1994년 1월 21일 (25세), 서울특별시 송파구
신체: 165cm, 51kg[3], B형
데뷔: 2013년 EP
학력: 꿈의학교(서산, 초등과정) → 뉴저지 레오니아 고교(美) → 늘푸른고등학교 → 송림고등학교 졸업
 종교: 개신교(장로회)
이전 소속사: 미스틱엔터테인먼트
이전 유통사: CJ E&M MUSIC(2013.07
 2015.03)
로엔엔터테인먼트(2015.04
 12)
 행동 시기: 2011년 ~ 2016년, 시재 은퇴(추정)
소속그룹: 투개월(보컬)
가족: 1남 1녀 중도 막내
 [준자작 작위]
 김유안(2001년)/한국의 배우
 이름: 김유안(Kim Yu-an)
출생: 2001년 3월 15일(19세)
국적: 대한민국
신체: 165cm, O형
학력: 안양예술고등학교 연극영화과 (재학)
가족: 부모님, 1남1녀 중앙 막내
데뷔: 2017년 EBS 1TV '생방송 톡!톡! 보니하니'
소속사: JYP엔터테인먼트
 [남작 작위]
 김유정(1999년)/한국의 배우
 이름: 김유정(金裕貞, Kim You-jung)
출생: 1999년 9월 22일, 서울특별시 성동구 금호동
국적: 대한민국
신체: 165cm, O형
가족: 부모님, 오빠 김부근(1993년생), 언니 김연정(1996년생), 육촌 오빠 권민제
학력: 서울금호초등학교 (전학) → 대명초등학교 (전학)
대화초등학교 (졸업)
대송중학교 (졸업)
홍익대학교사범대학부속여자고등학교 (전학)
고양예술고등학교 연기과 10기 (졸업)
 데뷔: 2003년 CF '크라운제과 - 크라운산도'
소속사: iHQ
종교: 개신교
 [준남작 작위]
 김윤혜(1991년)/한국의 여배우
 설명: 2017년 SBS 희곡 엽기적인 그녀에 출연하였고 2018년에 JTBC 희곡 제3의 매력에 출연했다.
 이름: 김윤혜(金允慧, 다른 허울_좋은_하눌타리[수박] 우리)
출생: 1991년 5월 24일(27세), 서울특별시 서초구 서초동
가족: 부모님, 2녀 한복판 둘째
신체: 168cm, 42kg
학력: 서이초등학교
서운중학교
양재고등학교
 소속: 이매진아시아
데뷔: 2002년 잡지 보그 걸 표지모델
 [남작 작위]
 김지민(2000년)/한국의 배우
 이름: 김지민(金志珉, Kim Ji-min)
출생: 2000년 2월 12일(18세)
신체: 162cm, 45kg
학력: 서울광진초등학교
봉은중학교
청담고등학교
중앙대학교 연극영화과 (재학)
 가족: 부모님
데뷔: 2008년 MBC 드라마 '달콤한 인생'
소속사: JYP엔터테인먼트
 [준남작 작위]
 김지숙/한국의 가수, MC/네이버 장르 블로그/탤런트/前 레인보우
 이름: 김지숙(金智淑)
출생: 1990년 7월 18일(28세), 경기도 수원시
국적: 대한민국
신체: 162.6cm, 46kg, A형
성좌/지지: 게자리/말띠
가족: 아버지, 언니
학력: 창현고등학교
한양여자대학교 실용음악학 학사
 데뷔: 2009년 레인보우 EP 앨범 Gossip Girl
별명: 볼매, 말괄량이, 쑥이, 파워블로거, 효녀지숙, 수원의 딸
포지션: 메인보컬
특기: 서예, 요리, 네일아트, 리폼
소속사: 디모스트엔터테인먼트
 [기사 작위]
 김지영/2016년 실착 대구 참가자
 미스대구 참가번호: 13번
나이: 만 23세
학력: 영남대학교 행정학과 3학년
특기: 수영
장래희망: 외교관
경력: 2016년 실례 대구 참가자
 [기사 작위]
 김지원/2017년 미스대구 선
 나이: 23
학력: 계명대학교 성악과 3학년
경력: 2017년 미스대구 라인 쉬메릭
 [준자작 작위]
 김지원/한국의 여배우
 이름: 김지원(金智媛)/Kim Ji-won
출생: 1992년 10월 19일 (26세), 서울특별시 금천구
국적: 대한민국
신체: 163.2cm, 45kg, A형
학력: 서울온수초등학교 (졸업)
경인중학교 (중퇴)
백암고등학교 (졸업)
동국대학교 연극학부 (재학)
 가족: 부모님, 언니(1990년생)
종교: 개신교
데뷔: 2010년 상업광고 '롤리팝'
소속사: 스타쉽엔터테인먼트(킹콩 by 스타쉽)
 [남작 작위]
 김지은(KIM JI EUN/1993년)/한국의 배우
 출생: 1993년 10월 9일(25세)
신체: 166cm
학력: 청주대학교 연극학과 학사
데뷔: 2016년 '박카스' CF
소속사: HB엔터테인먼트
경력: (2017)웹드라마: 회사를 관두는 최고의 순간(현 배역)
(2018)SBS: 착한마녀전(왕지영 배역)
(2018)KBS2: 러블리 호러블리(이수정 배역)
(2018)MBC: 붉은 월광 푸른 해(민주 배역)
 [남작 작위]
 김지은/한국의 단역배우
 경력: (2013)SBS: 바로 키운 아녀자 하나
(2013)KBS1: 지성이면 감천
(2014)SBS: 닥터 이방인(병원 이사장 오준규 비서 배역)
(2014)MBC: 왔다! 장보리
(2014)MBC 드라마넷: 스웨덴 세탁소(기준 여자친구 배역)
(2015)KBS2: 파랑새의 집(장태수 회장실 비서 배역)
(2017)광고: 울산 방문의 해
 [남작 작위]
 김진경(1997년)/대한민국 여인네 모델
 설명: 김진경은 대한민국의 부녀 모델, 연기자이다. 2012년 《도전! 수퍼모델 KOREA 3》로 데뷔했으며 준우승에 올랐으며, 이강 같이 출연했던 고소현과 아울러 에스팀 소속이 되었다.
 출생: 1997년 3월 3일 (21세), 대한민국/서울특별시
키: 172.5cm, 54kg B형
학력: 한림연예예술고등학교
방송: 안단테
가족: 부모님, 2녀 속 막내
데뷔: 2012년 OnStyle '도전 수퍼모델코리아 시즌3'
소속사: 에스팀 엔터테인먼트
 [남작 작위]
 김정진(1994년)/2015년 미스코리아 선
 출생: 1994년 5월 30일, 충청북도 청주시
국적: 대한민국
학력: 공주대학교 화학과
신체: 171.5cm, 56.9kg, B형
성좌/지지: 쌍둥이자리/개띠
경력: 2015년 미스코리아 선
2015년 오산 충북-세종 당선자
 [기사 작위]
 김정현/2016년 미스대구 참가자
 오류 대구 참가번호: 16번
나이: 만 27세
학력: 영남대학교 생명과학과 4학년
특기: 요리,한국전통무용,요가
장래희망: 글로벌패션CEO
경력: 2016년 미스대구 참가자
 [남작 작위]
 김주현/한국의 배우
 이름: 김주현
출생: 1987년 3월 10일
신체: 165cm, 46kg
학력: 동국대학교 연극영상학
데뷔: 2007년 영화 기담
소속사: 화이브라더스
 [남작 작위]
 김태리(1990년)/한국의 배우
 이름: 김태리(金泰梨)/Kim Taeri
출생: 1990년 4월 24일 (28세), 서울특별시
국적: 대한민국
신체: 166cm, 46kg, B형, 225mm
가족: 부모님, 할머니, 오빠(1988년생)
학력: 신현중학교 (졸업)
영신여자실업고등학교 디자인과 (졸업)
경희대학교 신문방송학 (졸업)
 종교: 무종교
데뷔: 2014년 더바디샵 CF
소속사: 제이와이드컴퍼니
 [준남작 작위]
 김태영(KIM TAE YEONG)/2017년 미스코리아 2차 통과자
 지역: 부산울산
나이: 만 24세
신체: 173.9cm / 53.8kg / 34-23-36
출신: 정보부산대학교 의류학과
취미: 풀루트 / 발레
특기: 뮤지컬 / 영어
장래희망: 패션디자이너
경력: 2017년 미스코리아 2차 통과자
 [준남작 작위]
 김한솔(KIM HANSOL)/2018년 미스코리아 대구 참가(참가번호 15번)
 나이: 23
학력: 계명대학교 미국학전공 3
특기: 풍경화, 운동
장래희망: 항공승무원
경력: 2018년 미스코리아 대구 참가(참가번호 15번)
 [준남작 작위]
 김향기/한국의 배우
 이름: 김향기(金香起, Kim Hyang-gi)
출생: 2000년 8월 9일(18세), 경기도 용인시 수지구
신체: 155cm, A형
학력: 홍천초등학교 (졸업)
용인홍천중학교 (졸업)
성복고등학교 (재학)
한양대학교 연극영화학과 (입학예정)
 가족: 부모님, 오빠
종교: 개신교
데뷔: 2003년 CF 광고 '파리바게트'
소속사: 나무엑터스
 [준남작 작위]
 김현희/2014년 미스코리아 대구 진
 건회 코리아 참가번호: 8
생년월일: 1990년 8월 2일
신체사항: 169.8cm, 46.9kg, 33-24-36
학력: 경북대학교 시각정보디자인학과
이화여자대학교 대학원 시각디자인과
 취미: 한국무용, 필라테스
특기: 디자인, 리폼
장래희망: 문화예술 CEO
경력: 2014년 미스코리아 대구 진
 [남작 작위]
 김희선/한국의 모델
 출생: 1997년 9월 5일
신체: 174cm
경력: 2016 F/W 서울패션위크 푸쉬버튼 모델
2015 S/S 서울패션위크 무홍, 슈퍼콤마비 모델
잡지 바자, 데이즈드코리아, 싱글즈 모델
 뮤직비디오: 2016년: 노데이 - Wait Up (Feat. 산체스)
2015년: Homme - 사랑이 아냐
 [준남작 작위]
 김희정(1992년)/한국의 배우
 출생: 1992년 4월 16일[1] (26세), 부산광역시
신체: 160cm, 43kg
학력: 까치울초등학교
신월중학교
명덕여자고등학교
중앙대학교 공연영상창작학부
 가족: 부모님
데뷔: 2000년 KBS 드라마 '꼭지'
소속사: YG 엔터테인먼트
 [기사 작위]
 김혜윤(1996년)/한국의 배우
 이름: 김혜윤(Kim Hye-yoon)
출생: 1996년 11월 10일(22세), 서울특별시
신체: 160cm
가족: 부모님, 언니
학력: 건국대학교 예술디자인대학 영화예술학과
데뷔: 2013년 KBS TV소설 '삼생이'
소속사: 컵엔터테인먼트
 [준자작 작위]
 결경/Kyulkyung/가수
 본명: 저우제충 (周洁琼/ Zhōu Jiéqióng)
출생: 1998년 12월 16일, 중국 저장성 타이저우 시
신체: 167cm, 48kg[4], O형
국적: 중국
가족: 아버지, 안어버이 양빈림(1975년생), 여동생, 남동생 주역
학력: 톈타이소학교 (졸업)
상하이음악학원 부속 중등 음악전문학교 (졸업)
서울공연예술고등학교 방송연예과 (졸업)
 소속사: 플레디스엔터테인먼트
소속그룹: 프리스틴, 프리스틴 V, I.O.I
 [남작 작위]
 경수진/탤런트, 영화배우
 출생: 1987년 12월 5일, 경기도 시흥시
성좌/지지: 사수자리/토끼띠
가족: 부모님, 오빠(1983년생
신체: 164cm, O형
학력: 장곡고등학교
남서울대학교 스포츠산업학(중퇴)
 종교: 무종교
데뷔: 2011년 SBS 희곡 '신기생뎐'
소속사: YG엔터테인먼트
데뷔: 2012년 KBS 드라마 '적도의 남자'
 [준자작 작위]
 구리나자/중국의 배우
 설명: 굴나자르, 중국식 이름: 꾸리나쟈얼 빠이허티야얼는 중화인민공화국의 위구르족 신분 안 배우 및 모델이다. 2015년 베이징 영화 학원을 졸업하였다.
 이름: 구리나자(古力娜扎, Gǔlìnàzhā)
실명: 귈네제르 베흐티야르(گۈلنەزەر بەختىيار: 위구르어)
구리나자얼 바이허티야얼(古力娜扎尔·拜合提亚尔: 중국어)
출생: 1992년 5월 2일 (26세), 중화인민공화국 우루무치시
키: 172cm
학력: 베이징 영화 학원
대리인: Tangren Media
 [준남작 작위]
 구윤희/2018년 미스코리아 경북 참가(참가번호 06번)
 나이: 23
학력: 대구대학교 유아특수교육과 졸업
특기: 요리
장래: 희망교사
경력: 2018년 미스코리아 경북 참가(참가번호 06번)
 [준남작 작위]
 구하라/가수, 탤런트
 출생: 1991년 1월 13일 (만 27세), 광주광역시 구미 치평동
신체: 163cm, 43kg | B형
그룹: 카라
소속사: 콘텐츠와이
학력: 성신여자대학교 미디어영상연기학과
데뷔: 2008년 카라 EP 앨범 '카라 1st'
경력: 2018.7 순천만세계동물영화제 홍보대사 사랑사람 3건
 [준남작 작위]
 고마쓰 아야카(小松彩夏/Ayaka Komatsu)/일본의 그라비아 아이돌/가수/배우/패션 모델
 설명: 고마쓰 아야카는 일본의 그라비아 아이돌, 배우, 패션 모델이다. 소속사는 어뮤즈이다. 이와테현 이치노세키 잠시 출신이다.
 본명: 小松 彩夏/こまつ あやか/Ayaka Komatsu
출생: 1986년 7월 23일(32세), 기저 이와테현 이치노세키시(Ichinoseki, Iwate, Japan)
Height: 160 cm (5 ft 3 in)
血液型: A型
Occupation: Actress/Gravure idol/Model
음반회사: WEATNU Records
앨범: I'll Be Here
 [준남작 작위]
 고민시(1995년)/한국의 여배우
 이름: 고민시(高旼視)
출생: 1995년 2월 15일(23세)
데뷔: 2016년 72초 극 시즌3
소속사: 미스틱엔터테인먼트
영화: (2018년)마녀(도명희 배역)
(2018년)tvN: 하늘에서 내리는 1억개의 별(임유리 배역)
 [남작 작위]
 고성희(1990년)/한국의 배우(미국출생)
 설명: 외교부 교수인 아버지로 인해 미국에서 태어나고 자랐다. 미국과 대한민국 이중국적을 갖고있는 그녀는 영어와 일본어 등에 능통하다고 밝혔다.
 이름: 고성희(高聖熙, Ko Sung-hee)
출생: 1990년 6월 21일(28세), 미국
국적: 대한민국/미국(복수국적)
신체: 170cm, A형
학력: 성균관대학교 연기예술학과(휴학)
가족: 부모님, 2녀 속 막내
데뷔: 2013년 영화 '분노의 윤리학'
소속사: 사람엔터테인먼트
 [준자작 작위]
 고아라(高아라)/한국의 배우
 출생: 1990년 2월 11일 (28세)/경상남도 진주시
국적: 대한민국
본관: 제주 고씨(濟州 髙氏) 장흥백파 23세손
신체: 167cm, 50kg#, A형
학력: 광주송정초등학교 (졸업)
광주송정중학교 (전학)
서울 청담중학교 (졸업)
서울 청담고등학교 (전학)
정신여자고등학교 (졸업)
중앙대학교 연극영화학 (학사)
 소속사: SM엔터테인먼트(2003 ~ 2016.11.30)
아티스트 컴퍼니(2017.01.10 ~)
 가족: 부모님, 남동생 (2001년생, 18세)
데뷔: 2003년 KBS2 극 반올림
종교: 개신교
 [준자작 작위]
 고윤정/한국의 여배우
 이름: 고윤정
출생: 1996년
신체: 167cm
학력: 서울여자대학교 현대미술과
소속사: MAA
데뷔: 2019년 드라마 '사이코메트리 그녀석'
인스타그램: https://www.instagram.com/goyounjung/
 [준자작 작위]
 고준희(高準熹, Go Jun-hee)/한국의 배우
 본명: 김은주(金恩珠)
출생: 1985년 8월 31일, 서울특별시
신체: 172cm, 50kg, A형
학력: 한가람고등학교
경희대학교 연극영화학(학사)
 가족: 부모님, 1남 1녀 속 첫째
데뷔: 2001년 'SK 스마트학생복' 모표 선발대회
소속사: YG엔터테인먼트
 [남작 작위]
 공서영/한국의 아나운서/前 여가수/예능인/MC/배우
 이름: 공서영(孔書英)
출생: 1982년 8월 9일 (36 세), 충청북도 영동군
본관: 곡부 공씨
신체: 166cm, 47kg, B형
데뷔: 2003년 KBS 미니시리즈 '로즈마리' OST
학력: 한광여자고등학교 졸업
소속: 디모스트엔터테인먼트
 [준남작 작위]
 공승연 (유승연)/탤런트
 출생: 1993년 2월 27일 (만 25세)
신체: 165cm, 44kg
소속사: BH엔터테인먼트
가족: 아버님 유창준, 제매 정연
학력: 성신여자대학교 미디어영상연기학과 졸업
데뷔: 2012년 CF 유한킴벌리 '화이트'
 [남작 작위]
 공유림/탤런트
 출생: 1996년 1월 13일 (만 22세)
소속사: 옐스콘텐츠
학력: 동국대학교 연극학부
 [남작 작위]
 나가노 메이/일본의 배우
 설명: 나가노 메이는 일본의 배우, 패션 모델이다. 도쿄도 출신. 스타더스트 프로모션 소속이다. 초등학교 3학년 재학 허리 기치조지에서 스카우트되어 연예계에 입문하여, 2009년 영화 <하드 리벤지, 밀리 블러드 배틀>에서 아역으로 데뷔했다. 2018년에는 NHK 아침드라마 <절반, 푸르다>의 히로인 스즈메 역으로 발탁되었다.
 본명: 나가노 메이 (永野 芽郁, Mei Nagano)
출생: 1999년 9월 24일 (19세), 밑바닥 도쿄도
별자리/십이지: 천칭자리, 토끼띠
신체: 163cm/AB형
활동: 배우(2009년 ~ )
본명: Mei Nagano
대리인: 스타더스트 프로모션(スターダストプロモーション)
 [후작 작위]★★★
 나나 (Nana)/가수
 본명: 임진아(林珍兒)
국적: 대한민국
출생: 1991년 9월 14일 (27세), 충청북도 청주시
신체: 170.6cm, 52.6kg, A형
가족: 부모님
학력: 청주 서원중학교 (졸업)
오창고등학교 (졸업)
서울종합예술실용학교 뷰티예술학부
 데뷔: 2009년 애프터스쿨 싱글 2집 《너 때문에》
포지션: 비주얼, 리드보컬
소속사: 플레디스엔터테인먼트
소속그룹: 애프터스쿨, 오렌지캬라멜, 애프터스쿨 레드
수상: 2018 제23회 춘사영화제 여자 인기상 겉면 5건
 [기사 작위]
 나리, Nari/모델/코스플레이어
 설명: 시공의군주 및 귀검 네이버 광고에 당로 등장하였으며 제반 홍보 활동에 진행하였으며, 이제 쥔아저씨 활발하게 활동중이다. 걸그룹 레이샤와 함께 삼국지마스터 피아르 활동을 진행하였다.
 성별: 여성
직업: 모해 스트리머
신장: 171cm
소속사: RZCOS(TeamRZ)
트위치 석방 시작: 2019. 2. 27
RZCOS 기동 시작: 2018. 8. 16
트위치: https://www.twitch.tv/nari_cosplay
인스타그램: https://www.instagram.com/nariddyang/
메일: dailynari@gmail.com
 [기사 작위]
 나이슬/한국의 모델/2016년 미스대구 참가자
 미스대구 참가번호 3번
나이: 만 29세
학력: 대신대학교 피아노과 졸
특기: 스쿼시
장래희망: 연기자
경력: 2016년 미스대구 참가자
 [준남작 작위]
 나카다 카나(中田花奈/Kana Nakada)/일본의 아이돌/노기자카46(1기생)
 본명: 나카다 카나(中田花奈/なかだ かな/Kana Nakada)
출생: 1994년 8월 6일 (24세), 자성 사이타마현
영화: Death Blog
신체: 키 158cm/A형
소속그룹: 노기자카46(1기생)
소속사: 노기자카46 합동회사
 [준자작 작위]
 남지현(1995년)/한국의 배우
 이름: 남지현(南志鉉)
출생: 1995년 9월 17일 (23세), 인천광역시 부평구
신체: 163cm, B형
학력: 인천백운초등학교
인천상정중학교
인천초은고등학교
서강대학교 심리학과 (재학)
 가족: 부모님
데뷔: 2004년 MBC 드라마 '사랑한다 말해줘'
소속사: 매니지먼트 숲
 [준후작 작위]★★
 니시노 나나세(西野 七瀬/Nanase Nishino)/일본의 아이돌/노기자카46
 설명: 니시노 나나세는 일본의 아이돌이며, 여성 아이돌 무리 노기자카46의 멤버 중간 한명이다.
 본명: (西野 七瀬/にしの ななせ/Nanase Nishino)
출생: 1994년 5월 25일, 성근 오사카부
신장: 159cm/O형
음반회사: 소니 뮤직 엔터테인먼트
앨범: 투명한 색, 각각의 의자, 태어나서 돈머릿수 본 꿈, Synchronicity (Special Edition), 나만의 너
 Under Super Best

소속 그룹: 노기자카46(2011년 ~ 2018년)
전속 모델: non-no(2015.02 ~ 현재)
소속사: 노기자카46 LLC
 [남작 작위]
 니시우치 마리야(西内 まりや)/일본의 배우, 가수, 모델
 설명: 소속사 라이징 프로덕션과는 2018년 3월 말에 계약이 종료된다. 태시 연예계 암묵의 룰인 '소속사에서 안식구 좋게 헤어진 연예인은 쓰지 않는다'라는 말을 생각해보면 앞으로의 연예계 생활이 험난해진 셈이다.
 본명: 니시우치 마리야(西内 まりやにしうち まりや, Mariya Nishiuchi)
출생: 1993년 12월 24일 (25세, 사수자리, 닭띠)
지역: 자질 후쿠오카현 후쿠오카시 주오구
국적: 일본
신체: 키170cm/B80cm-W58cm-H83cm/S250mm/A형
직업: 배우, 성우, 가수
소속: 프리랜서
