---
layout: post
title: "[스크랩] [펌] 재미로보는 혈액형별 골퍼..."
toc: true
---

 0형

 1. 숏게임보다, 롱게임에 능하고, 중요하게 여긴다.

 그중에도 드라이버샷을 골프의 꽃이라 생각한다.

 2. 롱홀에서 투온을 시도하며, 가끔 성공, 큰 희열을 맛본다.

 3. 그럼에도 실수도 많아, 골프를 망치는 경우도 많다.

 (골프前 속으로 다짐한것과는 반대로, 무리한 샷을 하곤한다.)

 4. 내기에서 판을 잘 키운다. (따따등으로..) 아주 잃거나, 딴다.

 5. 캐디에게 기분에 따라 팁도 상천 주지만, 오히려 신경질을 잘부린다.

 (욕설도 하는 경우가 있다)

 6. 그린 주변에서 상천 퍼터를 사용하는자를 경멸, 싫어한다.

 7. 항용 맞는 날과 망치는 날의 기복이 심하다. (10타이상)

 8. OB와 로스트볼이 많다. 버디도 뜻대로 잡고, 더블파 또는 그이상도 잘한다.

 9. Never up ,Never In을 신봉, 약간 길게 치며, 프로라인을 선호한다.

 (3퍼터 잘한다)

 10. 다양한 골프 무용담을 가지고있으며 혼외정사 확대하여 말하곤한다.

 11. 단시일내에 싱글이 되며, 목표의 상실감으로 골프와 멀어지기도 한다.

 그러면, 급격히 실력이 저하되는 경향이있다.

 (공격적 골프라 연습없으면 심한 하향곡선)

 [골프입스](https://bootsay.com/sports/post-00029.html) A형

 1. 롱게임 한층 숏게임을 즐기며 능하다.

 (페어웨이에서 채를 여러개 들고 다님)

 2. 내기에 약하며, 워낙 큰판에 약하다.

 (내기를 싫어한다)

 3. 어프로치나 퍼터가 섬세하나, 긴장으로 토핑, 더핑이 많다.

 4. 골프 경련증(입스) 증세가 자작 나타나는 성격이다.

 5. 캐디에게 정해진 수고비 이외에 빈빈히 안준다.

 라운딩전에 캐디피를 물어본다.

 (그리고 속으로 4등분 해본다, 돈계산은 확실)

 6. 그린 주변서 퍼터 사용을 즐기며, 그린 두름손 벙커에서도 퍼터를 사용한다.

 (웨지와 퍼터를 가지고 오랫동안 고민하다가.결국은..)

 7. 롱홀보다 숏홀, 평탄한 지형보다 산악지형을 선호한다.

 (코스레이팅 관계로 짧으니까)

 8. 미스샷후 직통 속으로 자책하고 괴로워한다.

 (아님 얼굴이 하얗게 변한다)

 9. 결정적 숏퍼팅 실수는 즉일 골프에 치명적인 요소가 된다.

 (심장마비 가능형, 충격으로 라운딩을 중도에 그만두기도 한다)

 10. 대조적 골프매너가 좋으며, 남에게 피해주는 행동은 안한다.

 11. 캐디가 선호하는타입이며, 진행이 늦으면 기연히 이사람에게 살며시 말한다.

 (그후로 혼자만 서둘다 미스샷을 낸다. 더구나 자책한다.)

 12. 골프 전날 불면에 시달리며 밤새 꿈속에서 라운딩한다.

 숙면 못해 컨디션이 나쁜 경우가 많다.

 13. 골프용품(공,티,장갑,모자,골프화)을 완벽하게 준비하며,

 먹을 물과 음식도 잘싸온다.

 14. 골프분석에 능하며, 골프룰, 메카니즘에 능통하다.

 허나 연습은 백날 안한다.(네가티브적 골프라 실력은 변화가 없는편.)

 15. 모자라고 짧은 퍼팅이 많고, 아마츄어라인으로 잘간다.

 16. 손수 점수와 남의 점수를 꿰 차고 있다. 샷 순서도 꼭지킨다.

 17. 스코어카드를 모으거나, 점수를 기록하고 분석을 즐긴다.

 18. 싱글문턱에서 많은 좌절이 있으며 싱글하는 시기가 늦다.

 19. 의외로 조급함과 경쟁심이 있으며 골프로 인한 스트레스가

 쌓일수 있다.

 B형

 1. 낭군 골프를 이내 칠 수 있는 혈액형

 2. 뻔쩍하면 잊어 먹는 체질이라, 퍼터를 놓고 온다든지 골프화를 안가지고 오는형.

 (티샷마다 티 작히 달라한다)

 3. 골프의 룰이나, 메카니즘은 무게 모르며, 완만한 리듬으로만 치는형.

 4. 실수후 후유증이 오래도록 안가며, 진행이 늦는형

 (인터벌이 긴게 아니고, 걸음걸이, 준비상황)

 5. 퍼터도 장성 긴장감없이 리듬을 가지고 잘하는편. 3퍼터도 장성 고통없이 잘함

 6. 내기에 강하며, 큰판은 더욱 잘한다. (큰판에 사이 흔들린다)

 7. O,A형에 비해 결정적 실수가 없으며, 대단히 망친홀이 드물다.

 따라서 골프의 점수가 좋다.

 8. 손수 점수도 모르고, 더더군다나 남의 점수도 모른다.

 티샷의 순서,차례도 모른다.(캐디가 차례를 알려준다)

 9. 골프 내기時 타인의 점수를 몰라 주는데로 받고, 달라는데로 준다.

 10. 무서운점도 있다.불친절캐디는 직통 말안하고, 슬슬 경기과에 태연하게

 신고한다.( 싸가지 없다고..웃으면서..)

 11. 순종적이고 골프로 덜 스트레스를 받는다.

 AB형

 1. 골프미학을 추구하며, 의도적인 훅, 슬라이스, 드로우, 페이드샷를 즐긴다.

 라이가 어려운 퍼팅에 능하다.

 2. 다양한 어프로치를 즐기고, 유머와 우스게소리, 더구나 Y담을 즐기고,

 캐디에 관심이 많다.(캐디 헨드폰번호 적실히 물어보는 타입)

 3. 좋은시간,날씨,좋은 동반자가 아님 라운딩을 사양하는경향,

 골프채를 자주자주 바꾼다.

 4. 골프컨디션을 내적(본인)보다 외적인요소로 이어 돌린다.

 (날씨,동반자,캐디,그린컨디션..등)

 

