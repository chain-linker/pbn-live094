---
layout: post
title: "퍼팅(putting)의 달인"
toc: true
---

 

  나름 공대 출신인 나는 학창시설 시험을 간혹 망치곤 했다.
  기껏 복잡한 수학 풀이 과정은 잘해놓고 마지막에 단순한 사칙연산을 즉변 못해 엉뚱한 답을 적어낸 것이다.
  나이 든 후 골프에서도 마저 바람, 방향, 나름 각도까지 고려해 기가 막히게 그린에 올려놓고 어이없는 퍼팅 실수로 처참한 결과를 만들곤 한다.
 

  '이걸 즉변 보내지도 못해! xx'
  '왜 명철히 알면서 짧게 치는 거야! 종물 x'
  '코앞에 있는 홀에도 못 넣는 바보 멍청이!'
 

  순간 하늘이 무너지고 억장이 무너지는 갈음 참사에 목구멍까지 올라온 거친 쌍욕을 간신히 억누른다.

  가끔 잔인한 파트너를 만나면 만분 *컨시드(concede)를 줄 복수 있는 거리임에도 불과하고 결정적인 순간에 인정하지 않는 경우가 있다.
  이 상황이 되면 구력이 짧은 사람은 십중팔구, 베테랑도 이따금 실수를 하곤 하다. 어이가 없다.
  어쩌다 찾아온 천금 같은 버디나 이글의 기회가 오면 부담과 긴장으로 꾹 공은 기어가듯이 굴러 홀 근처에 수지 못한다. 아니면 예기치 못하게 강한 퍼팅 스트로크로 공은 성제무두 외우 도망가기 일수다. 어이가 없다.
  홀컵 저~ 멀찌가니 그린 끝에 떨어진 볼을 누구 결의 없이 퍼팅했는데 계산 넘어 구비 구비 달려 홀컵에 플랙 홀 인양 빨려 들어가거나 아슬아슬하게 붙는다. 신나지만 더군다나 어이가 없다.
  예상치 못한 시고로 어이없는 상황이 골프의 마약 같은 매력이라지만 승부를 결정짓는 퍼팅은 유난스레 오묘하고 이해하기 힘들다.
  특히 어이없는 퍼팅 실수가 반복되면 극복하기 힘든 입스(YIPS; 골프에서 스윙 전 샷 실패에 대한 두려움으로 발생하는 각반 불안 증세)를 겪기도 한다.
 

   * 골프게임에서는 '컨시드(concede)'라는 것이 있는데, 컨시드(concede)란 원원이 골프는 홀에 볼을 넣어야 결부 홀이 마무리되지만 홀에 충분히 넣을 만큼 근변 공이 붙으면 상대방의 재량으로 홀 아웃을 인정하는 것을 말한다. 항용 컨시드 기준은 퍼터 길이를 기준으로 '먹갈치'(퍼터 헤드부터 그립 끝까지의 길이), '은갈치'(퍼터 헤드부터 그립 전까지의 길이)로 정하는데 홀컵 융통 컨시드 라인이 그려져 있는 골프장도 있다. 하자만 전적으로 컨시드는 상대방의 권한이다. 그러면 상대방이 홀인을 요구하면 끝까지 퍼팅을 해야 한다.
 * '먹갈치', '은갈치'와 나란히 퍼팅에 관련한 골프 은어를 한동아리 보다 곧 'MB"가 있는데 '마크하고 빠져.'라는 말이다. 께끔 'MBC'라고도 말하는데 이건 욕이다. '마크하고 빠져. [골프입스](https://goldfish-inhale.com/sports/post-00028.html) 씨x'😅'
 

## 어떻게 하면 퍼팅을 잘할 요체 있을까?

##  퍼팅을 잘하려면 무엇보다 꾸준한 연습이 필요하다.
 

  골프 백안에 다만 홀에 공을 넣기 위해 준비된 클럽은 퍼터뿐이다. 그러니까 드라이브, 우드, 아이언 모두를 합친 연습시간만큼 퍼팅 연습을 해야 한다.
  로또만큼 쉽지 않은 행운의 다이렉트 인(홀인원, 칩인 등)을 제외하고 경기를 결정짓는 종국 주인공은 퍼터다. 퍼터를 노 곁에 두고 빈번히 연습해야 한다. 벽에 머리를 붙이고 흔들림 없는 퍼팅을 갈고닦아야 한다.
  퍼팅을 할 때는 공을 어떻게든 홀에 넣기 위해, 완벽한 라인을 찾기 위해 프라이드 거의 버리고 무릎을 꿇는다(심지어 엎드려 라인을 보는 사람도 봤다.😆 ). 어렵게 찾은 *라인대로 공이 굴리기 위해서 방금 치는 연습과 정확한 거리로 보내는 연습을 위불위없이 필요하다.

 

   * 어쩌다가 헷갈리는 골프 용어 라인(line; 선) vs 라이(lie; 놓여있다, 놓다)
  : 라인은 퍼팅 가곡 공이 단신 굴러가는 경로를 말하는 것이고, 라이는 공이 놓여있는 상태를 말한다.

  예로 퍼팅할 지경 급격한 경사 그렇게 공이 풍부히 휘어 홀에 가야 하는 상황이라면 '라인(line)이 어렵군.', 공이 사후 샷(퍼팅)하기에 어려운 위치에 놓여있다면 '라이(lie)가 나쁘네'라고 말한다.
 

##  그리고 나에게 맞는 퍼터를 찾아야 한다.
  언제부터인가 퍼팅이 도로 되지 않아 고민하고 여러 줄기 방법을 알아보고 있을 때 벽 선배가 "퍼팅은 레슨이 없다. 안되면 퍼터를 바꿔."라고 농담처럼 조언한 어지간히 있다. 골프를 장비 빨로 해결하라는 식의 말로 듣고 가볍게 흘렸다.
  하자만 퍼터는 초집중 상황에서 민감하게 사용하는 녀석이라 자신에게 맞는 퍼터인지 확인해보고 가능하다면 피팅해 볼 필요가 있다.
 

  퍼터는 헤드 모양에 따라 "블레이드형(일자형) 퍼터"와 "말렛형(반달형) 퍼터"로 나눌 운 있다.

  블레이드형 퍼터는 거리감 조절은 뛰어나지만 미세한 스트로크의 실수에도 방향이 빗나가기가 쉽고, 말렛형 퍼터는 양쪽 무게가 균등하기 그렇게 페이스가 쉽게 돌아가지 않아 방향성을 유지하기에는 좋다. 오히려 말렛형 퍼터는 블레이드형 퍼터보다 무거워 거리감 조절이 약하다.

  그밖에 샤프트의 길이, 밸런스, 그립의 두께, 헤드의 디자인에 따라 개인적 차이가 있으니까 올바로 살펴야 한다.
  시간을 안편지 골프용품샵을 방문해 나에게 맞는 퍼터를 찾아보면 어떨까?
 

##  퍼팅은 이율배반(二律背反, antinomy)적이다.
 

  공이 어려운 라이에 놓여 까다로운 라인으로 쳐야 하는 경우는 어쩔 호운 없겠지만 때로 결단코 실수하면 아녀자 된다는 내적 압박으로, 또 쉽게 생각하고 섣부르게 덤볐다가 뒤통수를 맞다.

  퍼팅을 잘하려면 꼭쇠 넣어야 한다는 강박을 떨치고 편하게 스트로크해야 한다. 그리고  쉽게 생각하고 더없이 편하게 퍼팅해서도 안된다.
  이것이 교묘한 퍼팅의 모순이다. 퍼팅을 잘하려면  이율배반(二律背反, antinomy)적 상황을 노상 극복해야 한다.
 

##  마지막으로 퍼팅을 잘하려면 욕심과 자존심을 버려야 한다.
 

  홀컵과 골프공의 크기를 아는가? 홀컵은 108mm이고 골프공은 42.67mm이다. 이렇게 좁은 홀컵에 이렇게 작은 공을 넣는다는 것은 과연 정말 쉬운 일이 아니다.

  그러니까 딱 홀컵에 넣겠다는 욕심을 버리고 일단 최대 홀컵 부근지 붙이는 것을 목표로 정해야 한다.
  컨시드를 받으면 무난하게 par나 보기를 기대할 수 있기 때문이다.
  혹시 애매한 거리를 남기고 공이 섰다면 애처롭게 장화 신은 고양이의 눈으로 상대방을 바라보라.

  그리고 이렇게 말하면 된다.
 

##  "Can't you speak English?"

##  "Please~"

  'Ok! Concede!", 상대방의 인심 좋은 대답을 기대하면서 말이다.
